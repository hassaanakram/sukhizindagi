package com.sukhizindagi.app.ui.services

import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

class RetrofitClient {

    companion object {

        private const val baseUrl = "https://admin.sukhizindagi.com/"
        private var instance: Retrofit? = null

        fun getInstance(): Retrofit {
            if (instance == null) {

                val clientBuilder: OkHttpClient.Builder = OkHttpClient.Builder()
                clientBuilder.readTimeout(30, TimeUnit.SECONDS)
                clientBuilder.writeTimeout(30, TimeUnit.SECONDS)
                clientBuilder.connectTimeout(30, TimeUnit.SECONDS)

                val loggingInterceptor = HttpLoggingInterceptor()
                loggingInterceptor.level = HttpLoggingInterceptor.Level.BODY
                clientBuilder.addInterceptor(loggingInterceptor)

                instance = Retrofit.Builder().addConverterFactory(GsonConverterFactory.create()).baseUrl(baseUrl).client(clientBuilder.build()).build()
            }
            return instance!!
        }

    }
}