package com.sukhizindagi.app.ui;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Patterns;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.sukhizindagi.app.R;
import com.sukhizindagi.app.ui.helpers.Utils;
import com.sukhizindagi.app.ui.services.Result;
import com.sukhizindagi.app.ui.services.ServicesFactory;

import org.jetbrains.annotations.NotNull;

public class PhoneVerificationActivity extends AppCompatActivity implements View.OnClickListener, Result {
    com.sukhizindagi.app.ui.helpers.PinEntryEditText editText;
    Intent intent;
    String mobile, otpString;
    CardView sendVerification;
    Dialog dialog;
    String fromSignUp = null;
    Boolean setForResult = false;
    String date, time, maids, addressOfUserFromMap, password, category, jobTitle, hours,latitude,longitude;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_phone_verification);

        initView();
    }

    private void initView() {
        intent = getIntent();
        if (intent != null) {
            mobile = intent.getStringExtra("mobile");
            fromSignUp = intent.getStringExtra("fromSignUp");
            setForResult = intent.getBooleanExtra("setForResult", false);
            date = intent.getStringExtra("date");
            time = intent.getStringExtra("time");
            maids = intent.getStringExtra("maids");
            hours = intent.getStringExtra("hours");
            jobTitle = intent.getStringExtra("job_title");
            category = intent.getStringExtra("category");
            password = intent.getStringExtra("password");
            latitude = intent.getStringExtra("latitude");
            longitude = intent.getStringExtra("longitude");
            addressOfUserFromMap = intent.getStringExtra("addressOfUserFromMap");

        }
        editText = findViewById(R.id.two_tv);
        sendVerification = findViewById(R.id.reset_password_card);
        sendVerification.setOnClickListener(this);
        editText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length() == 4) {
                    veriftyOtp();
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });
    }

    private void veriftyOtp() {
        dialog = Utils.showLoadingDialog(this);
        otpString = editText.getText().toString();
        new ServicesFactory(this, 0).sendOtp(mobile, otpString);

    }

    private Boolean areInputsOkay() {

        return true;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.reset_password_card:
                dialog = Utils.showLoadingDialog(this);
                veriftyOtp();
                break;
        }
    }

    @Override
    public void onSuccess(@NotNull String data, int requestCode) {
        Utils.dismissDialog(dialog);

        if (setForResult) {
            setResult(Activity.RESULT_OK);
            Intent intentFinal = new Intent(PhoneVerificationActivity.this, PreviewBookingActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
            intentFinal.putExtra("date", date);
            intentFinal.putExtra("time", time);
            intentFinal.putExtra("maids", maids);
            intentFinal.putExtra("hours", hours);
            intentFinal.putExtra("job_title", jobTitle);
            intentFinal.putExtra("password", password);
            intentFinal.putExtra("addressOfUserFromMap", addressOfUserFromMap);
            intentFinal.putExtra("category", category);
            intentFinal.putExtra("longitude", longitude);
            intentFinal.putExtra("latitude", latitude);
            startActivity(intentFinal);
            finish();
        } else if (fromSignUp != null) {
            Intent intent = new Intent(PhoneVerificationActivity.this, ExploreServicesActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);

        } else {
            Intent intent = new Intent(PhoneVerificationActivity.this, ResetPasswordActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.putExtra("otp", otpString);
            intent.putExtra("mobile", mobile);
            startActivity(intent);
        }
    }

    @Override
    public void onFailure(@NotNull String cause, int requestCode) {
        Utils.dismissDialog(dialog);
        Toast.makeText(this, cause, Toast.LENGTH_SHORT).show();
    }
}
