package com.sukhizindagi.app.ui;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.style.UnderlineSpan;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseApp;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.google.gson.Gson;
import com.sukhizindagi.app.R;
import com.sukhizindagi.app.ui.helpers.Utils;
import com.sukhizindagi.app.ui.models.Example;
import com.sukhizindagi.app.ui.services.Result;
import com.sukhizindagi.app.ui.services.ServicesFactory;

import org.jetbrains.annotations.NotNull;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;

public class SignInActivity extends AppCompatActivity implements View.OnClickListener, Result {

    TextView forgotPasswordTv;
    CardView signInCard;
    EditText emailEtSignIn, passwordEtSignIn;
    String email_s, password_s, longitude, latitude;
    Boolean startedForResult = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_in);
        startedForResult = getIntent().getBooleanExtra("startedResult", false);
        initView();
        statusBar();
    }

    private void statusBar() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            getWindow().setStatusBarColor(getResources().getColor(R.color.colorPrimaryDark, this.getTheme()));
        } else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().setStatusBarColor(getResources().getColor(R.color.colorPrimaryDark));
        }
    }

    String app_id = "";
    String
            date,
            time,
            maids,
            hours,
            jobTitle,
            addressOfUserFromMap;
    int category;
    Intent intentForCheckUserActivity = null;

    private void initView() {
        intentForCheckUserActivity = getIntent();
        if (intentForCheckUserActivity != null) {
            category = intentForCheckUserActivity.getIntExtra("category", 0);
            date = intentForCheckUserActivity.getStringExtra("date");
            time = intentForCheckUserActivity.getStringExtra("time");
            maids = intentForCheckUserActivity.getStringExtra("maids");
            hours = intentForCheckUserActivity.getStringExtra("hours");
            jobTitle = intentForCheckUserActivity.getStringExtra("job_title");
            addressOfUserFromMap = intentForCheckUserActivity.getStringExtra("addressOfUserFromMap");
            latitude = intentForCheckUserActivity.getStringExtra("latitude");
            longitude = intentForCheckUserActivity.getStringExtra("longitude");

        }
        FirebaseApp.initializeApp(this);
        forgotPasswordTv = findViewById(R.id.forgot_password_signin);
        SpannableString underStr = new SpannableString(forgotPasswordTv.getText());
        underStr.setSpan(new UnderlineSpan(), 0, underStr.length(), Spanned.SPAN_INCLUSIVE_EXCLUSIVE);
        forgotPasswordTv.setText(underStr);
        forgotPasswordTv.setOnClickListener(this);
        signInCard = findViewById(R.id.signin_card);
        signInCard.setOnClickListener(this);
        emailEtSignIn = findViewById(R.id.username_tv);
        passwordEtSignIn = findViewById(R.id.password_tv_signin);
        FirebaseInstanceId.getInstance().getInstanceId()
                .addOnCompleteListener(new OnCompleteListener<InstanceIdResult>() {
                    @Override
                    public void onComplete(@NonNull Task<InstanceIdResult> task) {
                        if (!task.isSuccessful()) {
                            Log.w("", "getInstanceId failed", task.getException());
                            return;
                        }

                        // Get new Instance ID token
                        app_id = task.getResult().getToken();
                    }
                });

    }

    Dialog dialog;

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.forgot_password_signin:
                Intent intent = new Intent(this, EnterMobileNumberActivity.class);
                startActivity(intent);
                break;
            case R.id.signin_card:
                if (areInputsOkay()) {
                    dialog = Utils.showLoadingDialog(this);

                    new ServicesFactory(this, 0).signIn(email_s, password_s, "1", app_id);
                }
                break;
        }
    }

    private Boolean areInputsOkay() {
        if (TextUtils.isEmpty(emailEtSignIn.getText())) {
            emailEtSignIn.setError("Required");
            emailEtSignIn.requestFocus();
            return false;
        }
        else if (TextUtils.isEmpty(passwordEtSignIn.getText())) {
            passwordEtSignIn.setError("Required");
            passwordEtSignIn.requestFocus();
            return false;
        }

        password_s = passwordEtSignIn.getText().toString();
        email_s = emailEtSignIn.getText().toString();
        return true;
    }

    @Override
    public void onSuccess(@NotNull String data, int requestCode) {
        Gson gson = new Gson();


        try {
            JSONObject obj = new JSONObject(data);
            String userAsString = String.valueOf(obj);
            PreferenceManager.getDefaultSharedPreferences(this).edit().putString("user", userAsString).putString("password", password_s).apply();
        } catch (JSONException e) {
            e.printStackTrace();
        }

        Toast.makeText(this, "Successfully Logged In", Toast.LENGTH_SHORT).show();
        Utils.dismissDialog(dialog);
        if (startedForResult) {
            setResult(Activity.RESULT_OK);
            Intent intentFinal = new Intent(SignInActivity.this, PreviewBookingActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
            intentFinal.putExtra("date", date);
            intentFinal.putExtra("time", time);
            intentFinal.putExtra("maids", maids);
            intentFinal.putExtra("hours", hours);
            intentFinal.putExtra("job_title", jobTitle);
            intentFinal.putExtra("password", password_s);
            intentFinal.putExtra("addressOfUserFromMap", addressOfUserFromMap);
            intentFinal.putExtra("category", category);
            intentFinal.putExtra("longitude", longitude);
            intentFinal.putExtra("latitude", latitude);
            startActivity(intentFinal);
            finish();
        } else {
            Intent intent = new Intent(SignInActivity.this, ExploreServicesActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
            finish();
        }


    }

    @Override
    public void onFailure(@NotNull String cause, int requestCode) {
        Toast.makeText(this, "Email / Phone and password combination is not correct, please try again.", Toast.LENGTH_SHORT).show();
        Utils.dismissDialog(dialog);
    }
}
