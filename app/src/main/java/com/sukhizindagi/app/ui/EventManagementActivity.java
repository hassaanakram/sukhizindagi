package com.sukhizindagi.app.ui;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.cardview.widget.CardView;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.sukhizindagi.app.CommonGEMapActivity;
import com.sukhizindagi.app.GardeningActivity;
import com.sukhizindagi.app.R;
import com.sukhizindagi.app.ui.helpers.DialogHelper;

import static com.sukhizindagi.app.ui.fragments.MapsFragment.MY_PERMISSIONS_REQUEST_LOCATION;

public class EventManagementActivity extends AppCompatActivity implements View.OnClickListener {
    CardView proceedNextCard, checkCardOne, checkCardTwo,
            checkCardThree,
            checkCardFour;
    String categoryId;
    private EditText additionalDetails;
    private Toolbar toolbar;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_event_management);
        initView();
    }

    private void initView() {
        TextView birthdayTv, weddingTv, cateringTv, corporateTv;
        weddingTv = findViewById(R.id.weddingTv);
        birthdayTv = findViewById(R.id.birthdayTv);
        cateringTv = findViewById(R.id.cateringTv);
        corporateTv = findViewById(R.id.corporateTv);
        weddingTv.setOnClickListener(this);
        birthdayTv.setOnClickListener(this);
        cateringTv.setOnClickListener(this);
        corporateTv.setOnClickListener(this);
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(EventManagementActivity.this, ExploreServicesActivity.class);
                startActivity(intent);
                finish();
            }
        });
        additionalDetails = findViewById(R.id.additionalDetailsEt);
        proceedNextCard = findViewById(R.id.proceedNext);
        proceedNextCard.setOnClickListener(this);
        checkCardOne = findViewById(R.id.checkCardOne);
        checkCardOne.setOnClickListener(this);
        checkCardTwo = findViewById(R.id.checkCardTwo);
        checkCardTwo.setOnClickListener(this);
        checkCardThree = findViewById(R.id.checkCardThree);
        checkCardThree.setOnClickListener(this);
        checkCardFour = findViewById(R.id.checkCardFour);
        checkCardFour.setOnClickListener(this);
    }

    private String jobType;
    private String additionalDetailsString = null;

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.proceedNext:

                if (jobType == null) {
                    Toast.makeText(this, "Kindly select Quote type", Toast.LENGTH_SHORT).show();
                } else {
                    if (!checkLocationPermission()) {
                        return;
                    }
                    Intent intent = new Intent(EventManagementActivity.this, CommonGEMapActivity.class);
                    intent.putExtra("jobType", jobType);
                    intent.putExtra("categoryId", categoryId);

                    if (TextUtils.isEmpty(additionalDetails.getText())) {
                        intent.putExtra("additionalDetailsString", additionalDetailsString);
                    } else {
                        intent.putExtra("additionalDetailsString", additionalDetails.getText().toString());
                    }
                    startActivity(intent);
                }
                break;
            case R.id.weddingTv:
            case R.id.checkCardOne:
                checkCardTwo.setCardBackgroundColor(Color.parseColor("#ffffff"));
                checkCardOne.setCardBackgroundColor(Color.parseColor("#008598"));
                checkCardThree.setCardBackgroundColor(Color.parseColor("#ffffff"));
                checkCardFour.setCardBackgroundColor(Color.parseColor("#ffffff"));
                jobType = "lawn development";
                categoryId = "12";
                break;
            case R.id.birthdayTv:
            case R.id.checkCardTwo:
                checkCardTwo.setCardBackgroundColor(Color.parseColor("#008598"));
                checkCardOne.setCardBackgroundColor(Color.parseColor("#ffffff"));
                checkCardFour.setCardBackgroundColor(Color.parseColor("#ffffff"));
                checkCardThree.setCardBackgroundColor(Color.parseColor("#ffffff"));
                jobType = "Kitchen gardening";
                categoryId = "13";
                break;
            case R.id.cateringTv:
            case R.id.checkCardThree:
                checkCardThree.setCardBackgroundColor(Color.parseColor("#008598"));
                jobType = "Farm house";
                checkCardTwo.setCardBackgroundColor(Color.parseColor("#ffffff"));
                checkCardOne.setCardBackgroundColor(Color.parseColor("#ffffff"));
                checkCardFour.setCardBackgroundColor(Color.parseColor("#ffffff"));
                categoryId = "14";
                break;
            case R.id.corporateTv:
            case R.id.checkCardFour:
                checkCardFour.setCardBackgroundColor(Color.parseColor("#008598"));
                checkCardThree.setCardBackgroundColor(Color.parseColor("#ffffff"));
                checkCardTwo.setCardBackgroundColor(Color.parseColor("#ffffff"));
                checkCardOne.setCardBackgroundColor(Color.parseColor("#ffffff"));
                jobType = "Corporate";
                categoryId = "15";
                break;
        }
    }

    public boolean checkLocationPermission() {
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED && ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            DialogHelper.showPermissionDialog(this, new DialogHelper.DialogButtonsListener<Boolean>() {
                @Override
                public void positiveButtonClick(Boolean aBoolean) {
                    ActivityCompat.requestPermissions(EventManagementActivity.this,
                            new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION},
                            MY_PERMISSIONS_REQUEST_LOCATION);
                }

                @Override
                public void negativeButtonClick(Boolean aBoolean) {

                }

                @Override
                public void canceled(Boolean aBoolean) {

                }
            });
            return false;
        } else {
            return true;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_LOCATION: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    // permission was granted, yay! Do the
                    // location-related task you need to do.
                    if (ContextCompat.checkSelfPermission(this,
                            Manifest.permission.ACCESS_FINE_LOCATION)
                            == PackageManager.PERMISSION_GRANTED) {
                        Intent intent = new Intent(this, CommonGEMapActivity.class);

                        startActivity(intent);
                        //Request location updates:
                    }

                } else {
                    checkLocationPermission();

                }
                return;
            }

        }
    }
}
