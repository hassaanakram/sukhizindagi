package com.sukhizindagi.app.ui.services

import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.http.*

interface EndPoints {


    @POST("android/1.0/register.php")
    fun signUp(
            @Query("email") email: String, @Query("password") password: String, @Query("device_type") device_type: String, @Query(
                    "app_id") app_id: String, @Query("mobile") mobile: String, @Query("first_name") firstname: String, @Query("last_name") lastname: String
    ): Call<ResponseBody>

    @FormUrlEncoded
    @POST("android/1.0/post-job.php")
    fun postJob(
            @Field("username") username: String, @Field("password") password: String, @Field("category_id") category_id: String, @Field(
                    "job_title") job_title: String, @Field("total_hrs") total_hrs: String, @Field("total_maids") total_maids: String, @Field(
                    "subscription_type") subscription_type: String, @Field("preferred_date") preferred_date: String, @Field("preferred_time") preferred_time: String, @Field(
                    "address") address: String, @Field("latitude") latitude: String, @Field("longitude") longitude: String
    ): Call<ResponseBody>

@FormUrlEncoded
    @POST("android/1.0/post-quote.php")
    fun postQuote(
            @Field("username") username: String, @Field("password") password: String, @Field("category_id") category_id: String, @Field(
                    "job_details") job_details: String, @Field(
                    "address") address: String, @Field("latitude") latitude: String, @Field("longitude") longitude: String
    ): Call<ResponseBody>

    @FormUrlEncoded
    @POST("android/1.0/login-check.php")
    fun signIn(
            @Field("username") email: String, @Field("password") password: String, @Field("device_type") device_type: String, @Field(
                    "app_id") app_id: String
    ): Call<ResponseBody>

    @FormUrlEncoded
    @POST("android/1.0/change-password.php")
    fun changePassword(
            @Field("username") email: String, @Field("password") password: String, @Field(
                    "new_password") newPassword: String
    ): Call<ResponseBody>

    @FormUrlEncoded
    @POST("android/1.0/forgot-password.php")
    fun forgetPassword(
            @Field("mobile") email: String
    ): Call<ResponseBody>

    @FormUrlEncoded
    @POST("android/1.0/verify-otp.php")
    fun sendOtp(
            @Field("mobile") email: String, @Field("otp") otp: String
    ): Call<ResponseBody>

    @FormUrlEncoded
    @POST("android/1.0/ongoing-jobs.php")
    fun ongoingJobs(
            @Field("username") email: String, @Field("password") password: String
    ): Call<ResponseBody>

    @FormUrlEncoded
    @POST("android/1.0/feedback.php")
    fun feedBack(
            @Field("username") email: String, @Field("password") password: String, @Field("job_id") jobid: String, @Field("rating") rating: String, @Field("rating_note") feedBackString: String
    ): Call<ResponseBody>

    @FormUrlEncoded
    @POST("android/1.0/past-jobs.php")
    fun pastJobs(
            @Field("username") email: String, @Field("password") password: String
    ): Call<ResponseBody>

    @FormUrlEncoded
    @POST("android/1.0/quotes.php")
    fun getQuotes(
            @Field("username") email: String, @Field("password") password: String
    ): Call<ResponseBody>


    @FormUrlEncoded
    @POST("android/1.0/reset-password.php")
    fun resetPassword(
            @Field("mobile") email: String, @Field("otp") otp: String, @Field("new_password") password: String
    ): Call<ResponseBody>

    @GET("restaurants/getAllCuisines")
    fun getAllCuisines(): Call<ResponseBody>

    @GET("restaurants/getFeaturedRestaurants")
    fun getFeaturedRestaurants(@Query("cityId") cityId: String): Call<ResponseBody>

    @GET("restaurants/getAllRestaurants")
    fun getAllRestaurants(): Call<ResponseBody>

    @GET("restaurants/search_ajax")
    fun getRestaurantsByOptions(
            @Query("text") name: String, @Query("date") date: String,
            @Query("timings") time: String, @Query("capacity") persons: String
    ): Call<ResponseBody>


}