package com.sukhizindagi.app.ui.adapters;

import android.content.Context;
import android.content.Intent;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.style.UnderlineSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.sukhizindagi.app.R;
import com.sukhizindagi.app.ui.QuoteDetailsActivity;
import com.sukhizindagi.app.ui.helpers.Utils;
import com.sukhizindagi.app.ui.models.JobsModel;
import com.sukhizindagi.app.ui.models.QuoteModel;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

public class QuotesAdapter extends RecyclerView.Adapter<QuotesAdapter.QuoteViewHolder> {

    private List<QuoteModel> dataSet;
    Context mContext;

    @NonNull
    @Override
    public QuoteViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.single_quote_item, parent, false);
        return new QuoteViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull QuoteViewHolder holder, int position) {
        final QuoteModel model = dataSet.get(position);
        holder.tvTitle.setText(model.getCategoryName());
        SpannableString spannableString = new SpannableString(holder.tvTitle.getText());
        spannableString.setSpan(new UnderlineSpan(), 0, spannableString.length(), Spanned.SPAN_INCLUSIVE_EXCLUSIVE);
        holder.tvTitle.setText(spannableString);
        holder.tvDate.setText(model.getCreatedOn());
        holder.tvStatus.setText(model.getStatus());
        holder.tvNote.setText(model.getDetails());
        if(model.getPrice() == null || model.getPrice().isEmpty()) {
            holder.tvPrice.setText("N/A");
        } else {
            holder.tvPrice.setText("Pkr. " + model.getPrice());
        }
        if(model.getJobTime() != null && !model.getJobTime().isEmpty()) {
            holder.tvTime.setText(model.getJobTime());
        }

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mContext.startActivity(new Intent(mContext, QuoteDetailsActivity.class).putExtra("QuoteModel", model));
            }
        });

    }

    @Override
    public int getItemCount() {
        return dataSet == null ? 0 : dataSet.size();
    }

    public QuotesAdapter(List<QuoteModel> data, Context context) {
        this.dataSet = data;
        this.mContext = context;

    }

   public class QuoteViewHolder extends RecyclerView.ViewHolder {

        private TextView tvTitle, tvPrice, tvStatus, tvTime, tvDate, tvNote;

       public QuoteViewHolder(@NonNull View itemView) {
           super(itemView);
           tvTitle = itemView.findViewById(R.id.tvTitle);
           tvPrice = itemView.findViewById(R.id.tvPrice);
           tvStatus = itemView.findViewById(R.id.tvStatus);
           tvTime = itemView.findViewById(R.id.tvTime);
           tvDate = itemView.findViewById(R.id.tvDate);
           tvNote = itemView.findViewById(R.id.tvNote);

       }
   }
}