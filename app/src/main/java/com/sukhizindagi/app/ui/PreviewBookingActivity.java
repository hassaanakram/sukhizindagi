package com.sukhizindagi.app.ui;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.cardview.widget.CardView;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.sukhizindagi.app.R;
import com.sukhizindagi.app.ui.helpers.DialogHelper;
import com.sukhizindagi.app.ui.helpers.Utils;
import com.sukhizindagi.app.ui.models.Example;
import com.sukhizindagi.app.ui.models.User;
import com.sukhizindagi.app.ui.services.Result;
import com.sukhizindagi.app.ui.services.ServicesFactory;

import org.jetbrains.annotations.NotNull;
import org.json.JSONObject;


public class PreviewBookingActivity extends AppCompatActivity implements View.OnClickListener, Result {
    Toolbar toolbar;
    Intent intentfromchoseactivity;
    ImageView taskDetailsIv, locationDetailsIv, contactDetailsIV;
    String date, time, maids, hours, job_title, email, password, addressOfUserFromMap,latitude,longitude;
    int category;
    CardView placeBooking;
    TextView hours_required_dynamic_tv,
            maids_required_dynamic_tv,
            cleaning_required_dynamic_tv,
            selected_date_dynamic_tv,
            prefered_time_dynamic_tv,
            address_dynamic_tv,
            customer_name_dynamic_tv,
            customer_email_dynamic_tv,
            customer_phone_dynamic_tv;
    User userObj;
    Gson gson = new Gson();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_preview_booking);
        password = PreferenceManager.getDefaultSharedPreferences(this).getString("password", null);

        try {
            String userString = PreferenceManager.getDefaultSharedPreferences(this).getString("user", null);
            JSONObject jsonObject = new JSONObject(userString);
            userObj = gson.fromJson(jsonObject.getString("user"), User.class);
        } catch (Exception e) {
            e.printStackTrace();
        }
        initView();
    }

    private void initView() {
        intentfromchoseactivity = getIntent();
        hours_required_dynamic_tv = findViewById(R.id.hours_required_dynamic_tv);
        maids_required_dynamic_tv = findViewById(R.id.maids_required_dynamic_tv);
        cleaning_required_dynamic_tv = findViewById(R.id.cleaning_required_dynamic_tv);
        selected_date_dynamic_tv = findViewById(R.id.selected_date_dynamic_tv);
        prefered_time_dynamic_tv = findViewById(R.id.prefered_time_dynamic_tv);
        address_dynamic_tv = findViewById(R.id.address_dynamic_tv);
        customer_name_dynamic_tv = findViewById(R.id.customer_name_dynamic_tv);
        customer_email_dynamic_tv = findViewById(R.id.customer_email_dynamic_tv);
        customer_phone_dynamic_tv = findViewById(R.id.customer_phone_dynamic_tv);

        if (intentfromchoseactivity != null) {
            category = intentfromchoseactivity.getIntExtra("category", 0);
            date = intentfromchoseactivity.getStringExtra("date");
            time = intentfromchoseactivity.getStringExtra("time");
            maids = intentfromchoseactivity.getStringExtra("maids");
            hours = intentfromchoseactivity.getStringExtra("hours");
            job_title = intentfromchoseactivity.getStringExtra("job_title");
            addressOfUserFromMap = intentfromchoseactivity.getStringExtra("addressOfUserFromMap");
            latitude = intentfromchoseactivity.getStringExtra("latitude");
            longitude = intentfromchoseactivity.getStringExtra("longitude");

        }
        toolbar = findViewById(R.id.toolbar_preview_booking);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(PreviewBookingActivity.this, ChoseLocationActivity.class);
                startActivity(intent);
                finish();
            }
        });
        taskDetailsIv = findViewById(R.id.edit_task_details_iv);
        locationDetailsIv = findViewById(R.id.edit_location_details_iv);
        contactDetailsIV = findViewById(R.id.edit_contact_details_iv);
        taskDetailsIv.setOnClickListener(this);
        locationDetailsIv.setOnClickListener(this);
        contactDetailsIV.setOnClickListener(this);

        hours_required_dynamic_tv.setText(hours);
        maids_required_dynamic_tv.setText(maids);
        selected_date_dynamic_tv.setText(date);
        prefered_time_dynamic_tv.setText(time);
        address_dynamic_tv.setText(addressOfUserFromMap);
        customer_name_dynamic_tv.setText(userObj.getDisplayName());
        customer_email_dynamic_tv.setText(userObj.getEmail());
        customer_phone_dynamic_tv.setText(userObj.getMobile());


        placeBooking = findViewById(R.id.place_booking_card);
        placeBooking.setOnClickListener(this);

    }

    Intent intent;

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.edit_task_details_iv:
                intent = new Intent(PreviewBookingActivity.this, TaskDetailsActivity.class);
                startActivity(intent);
                finish();
                break;
            case R.id.edit_location_details_iv:
                intent = new Intent(PreviewBookingActivity.this, ChoseLocationActivity.class);
                intent.putExtra("date", date);
                intent.putExtra("time", time);
                intent.putExtra("maids", maids);
                intent.putExtra("hours", hours);
                intent.putExtra("job_title", job_title);
                intent.putExtra("email", email);
                intent.putExtra("password", password);
                intent.putExtra("addressOfUserFromMap", addressOfUserFromMap);
                intent.putExtra("category", category);
                intent.putExtra("longitude", longitude);
                intent.putExtra("latitude", latitude);
                startActivity(intent);
                finish();
                break;
            case R.id.edit_contact_details_iv:
                intent = new Intent(PreviewBookingActivity.this, SettingsActivity.class);
                startActivity(intent);
                finish();
                break;
            case R.id.place_booking_card:
                dialog = Utils.showLoadingDialog(this);
                new ServicesFactory(this, 0).postJob(userObj.getEmail(), String.valueOf(category), password, job_title, hours, maids, "one-time", date, time, addressOfUserFromMap, latitude, longitude);
                break;
        }
    }

    Dialog dialog;

    @Override
    public void onSuccess(@NotNull String data, int requestCode) {
        Utils.dismissDialog(dialog);
        DialogHelper alert = new DialogHelper();
        alert.showDialog(PreviewBookingActivity.this);

    }

    @Override
    public void onFailure(@NotNull String cause, int requestCode) {
        Utils.dismissDialog(dialog);
        Toast.makeText(this, cause, Toast.LENGTH_SHORT).show();

    }
}
