package com.sukhizindagi.app.ui;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.tabs.TabLayout;
import com.google.gson.Gson;
import com.sukhizindagi.app.R;
import com.sukhizindagi.app.ui.adapters.QuotesAdapter;
import com.sukhizindagi.app.ui.fragments.OnGoingJobsFragment;
import com.sukhizindagi.app.ui.fragments.PastJobsFragment;
import com.sukhizindagi.app.ui.helpers.Utils;
import com.sukhizindagi.app.ui.models.QuoteModel;
import com.sukhizindagi.app.ui.models.User;
import com.sukhizindagi.app.ui.services.Result;
import com.sukhizindagi.app.ui.services.ServicesFactory;

import org.jetbrains.annotations.NotNull;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

public class QuotesActivity extends AppCompatActivity implements Result {

    Toolbar toolbar;
    TextView topBarText;
    Dialog dialog;
    User userObj;
    RecyclerView rvQuotes;
    Gson gson = new Gson();
    QuotesAdapter quotesAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_quotes);
        initView();
    }

    private void initView() {
        toolbar = findViewById(R.id.toolbar_preview_booking);
        rvQuotes = findViewById(R.id.rvQuotes);
        rvQuotes.setLayoutManager(new LinearLayoutManager(this));
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(QuotesActivity.this, ExploreServicesActivity.class);
                startActivity(intent);
                finish();
            }
        });
        topBarText = findViewById(R.id.toolbar_title);
        topBarText.setText("Quotes");
        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
        try {
            String userString = PreferenceManager.getDefaultSharedPreferences(this).getString("user", null);
            JSONObject jsonObject = new JSONObject(userString);
            userObj = gson.fromJson(jsonObject.getString("user"), User.class);
        } catch (Exception e) {
            e.printStackTrace();
        }
        String password = PreferenceManager.getDefaultSharedPreferences(this).getString("password", "password");
        dialog = Utils.showLoadingDialog(this);
        new ServicesFactory(this, 0).getQuotes(userObj.getEmail(), password);
    }

    List<QuoteModel> quoteModelArrayList = new ArrayList<QuoteModel>();
    @Override
    public void onSuccess(@NotNull String data, int requestCode) {
        Utils.dismissDialog(dialog);
        Gson gson = new Gson();
        try {
            JSONArray jobs = new JSONArray(data);
            String jobsString = String.valueOf(jobs);
            quoteModelArrayList = Arrays.asList(gson.fromJson(jobsString, QuoteModel[].class));
            quotesAdapter = new QuotesAdapter(quoteModelArrayList, QuotesActivity.this);
            rvQuotes.setAdapter(quotesAdapter);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onFailure(@NotNull String cause, int requestCode) {
        Toast.makeText(this, "No Record Found!", Toast.LENGTH_SHORT).show();
        Utils.dismissDialog(dialog);
    }
}
