
package com.sukhizindagi.app.ui.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class JobsModel implements Serializable {

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("unique_id")
    @Expose
    private String uniqueId;
    @SerializedName("user_id")
    @Expose
    private String userId;
    @SerializedName("job_posted_on")
    @Expose
    private String jobPostedOn;
    @SerializedName("category_id")
    @Expose
    private String categoryId;
    @SerializedName("job_title")
    @Expose
    private String jobTitle;
    @SerializedName("job_description")
    @Expose
    private String jobDescription;
    @SerializedName("job_status")
    @Expose
    private String jobStatus;
    @SerializedName("expert_id")
    @Expose
    private String expertId;
    @SerializedName("job_allocated")
    @Expose
    private Object jobAllocated;
    @SerializedName("complete_flag")
    @Expose
    private String completeFlag;
    @SerializedName("user_review_text")
    @Expose
    private Object userReviewText;
    @SerializedName("rating_note")
    @Expose
    private Object userRating;
    @SerializedName("rating")
    @Expose
    private String userReviewedOn;
    @SerializedName("invoice_no")
    @Expose
    private String invoiceNo;
    @SerializedName("paid_status")
    @Expose
    private String paidStatus;
    @SerializedName("expert_paid")
    @Expose
    private String expertPaid;
    @SerializedName("min_value")
    @Expose
    private String minValue;
    @SerializedName("max_value")
    @Expose
    private String maxValue;
    @SerializedName("timing")
    @Expose
    private String timing;
    @SerializedName("latitude")
    @Expose
    private String latitude;
    @SerializedName("longitude")
    @Expose
    private String longitude;
    @SerializedName("address")
    @Expose
    private String address;
    @SerializedName("expert_date")
    @Expose
    private Object expertDate;
    @SerializedName("expert_timing")
    @Expose
    private Object expertTiming;
    @SerializedName("job_amount")
    @Expose
    private String jobAmount;
    @SerializedName("job_cur_state")
    @Expose
    private String jobCurState;
    @SerializedName("user_date")
    @Expose
    private String userDate;
    @SerializedName("confirm_date")
    @Expose
    private String confDate;
    @SerializedName("confirm_time")
    @Expose
    private String confTime;
    @SerializedName("job_cancel_stat")
    @Expose
    private String jobCancelStat;
    @SerializedName("job_payment_stat")
    @Expose
    private String jobPaymentStat;
    @SerializedName("on_update")
    @Expose
    private String onUpdate;
    @SerializedName("notes")
    @Expose
    private Object notes;
    @SerializedName("dl")
    @Expose
    private String dl;
    @SerializedName("total_hrs")
    @Expose
    private String totalHrs;
    @SerializedName("total_persons")
    @Expose
    private String totalPersons;
    @SerializedName("username")
    @Expose
    private String username;
    @SerializedName("expert_name")
    @Expose
    private Object expertName;
    @SerializedName("expert_rating")
    @Expose
    private String expertRating;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUniqueId() {
        return uniqueId;
    }

    public void setUniqueId(String uniqueId) {
        this.uniqueId = uniqueId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getJobPostedOn() {
        return jobPostedOn;
    }

    public void setJobPostedOn(String jobPostedOn) {
        this.jobPostedOn = jobPostedOn;
    }

    public String getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    public String getJobTitle() {
        return jobTitle;
    }

    public void setJobTitle(String jobTitle) {
        this.jobTitle = jobTitle;
    }

    public String getJobDescription() {
        return jobDescription;
    }

    public void setJobDescription(String jobDescription) {
        this.jobDescription = jobDescription;
    }

    public String getJobStatus() {
        return jobStatus;
    }

    public void setJobStatus(String jobStatus) {
        this.jobStatus = jobStatus;
    }

    public String getExpertId() {
        return expertId;
    }

    public void setExpertId(String expertId) {
        this.expertId = expertId;
    }

    public Object getJobAllocated() {
        return jobAllocated;
    }

    public void setJobAllocated(Object jobAllocated) {
        this.jobAllocated = jobAllocated;
    }

    public String getCompleteFlag() {
        return completeFlag;
    }

    public void setCompleteFlag(String completeFlag) {
        this.completeFlag = completeFlag;
    }

    public Object getUserReviewText() {
        return userReviewText;
    }

    public void setUserReviewText(Object userReviewText) {
        this.userReviewText = userReviewText;
    }

    public Object getUserRating() {
        return userRating;
    }

    public void setUserRating(Object userRating) {
        this.userRating = userRating;
    }

    public String getUserReviewedOn() {
        return userReviewedOn;
    }

    public void setUserReviewedOn(String userReviewedOn) {
        this.userReviewedOn = userReviewedOn;
    }

//    public Object getExpertReviewText() {
//        return expertReviewText;
//    }
//
//    public void setExpertReviewText(Object expertReviewText) {
//        this.expertReviewText = expertReviewText;
//    }
//
//    public String getExpertRating() {
//        return expertRating;
//    }
//
//    public void setExpertRating(String expertRating) {
//        this.expertRating = expertRating;
//    }
//
//    public String getExpertReviewedOn() {
//        return expertReviewedOn;
//    }
//
//    public void setExpertReviewedOn(String expertReviewedOn) {
//        this.expertReviewedOn = expertReviewedOn;
//    }

    public String getInvoiceNo() {
        return invoiceNo;
    }

    public void setInvoiceNo(String invoiceNo) {
        this.invoiceNo = invoiceNo;
    }

    public String getPaidStatus() {
        return paidStatus;
    }

    public void setPaidStatus(String paidStatus) {
        this.paidStatus = paidStatus;
    }

    public String getExpertPaid() {
        return expertPaid;
    }

    public void setExpertPaid(String expertPaid) {
        this.expertPaid = expertPaid;
    }

    public String getMinValue() {
        return minValue;
    }

    public void setMinValue(String minValue) {
        this.minValue = minValue;
    }

    public String getMaxValue() {
        return maxValue;
    }

    public void setMaxValue(String maxValue) {
        this.maxValue = maxValue;
    }

    public String getTiming() {
        return timing;
    }

    public void setTiming(String timing) {
        this.timing = timing;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Object getExpertDate() {
        return expertDate;
    }

    public void setExpertDate(Object expertDate) {
        this.expertDate = expertDate;
    }

    public Object getExpertTiming() {
        return expertTiming;
    }

    public void setExpertTiming(Object expertTiming) {
        this.expertTiming = expertTiming;
    }

    public String getJobAmount() {
        return jobAmount;
    }

    public void setJobAmount(String jobAmount) {
        this.jobAmount = jobAmount;
    }

    public String getJobCurState() {
        return jobCurState;
    }

    public void setJobCurState(String jobCurState) {
        this.jobCurState = jobCurState;
    }

    public String getConfDate() {
        return confDate;
    }

    public void getConfDate(String userDate) {        this.userDate = confDate;    }

    public String getConfTime() {
        return confTime;
    }

    public void getConfTime(String userDate) {        this.userDate = confTime;    }

    public String getUserDate() {
        return userDate;
    }

    public void setUserDate(String userDate) {
        this.userDate = userDate;
    }

    public String getJobCancelStat() {
        return jobCancelStat;
    }

    public void setJobCancelStat(String jobCancelStat) {
        this.jobCancelStat = jobCancelStat;
    }

    public String getJobPaymentStat() {
        return jobPaymentStat;
    }

    public void setJobPaymentStat(String jobPaymentStat) {
        this.jobPaymentStat = jobPaymentStat;
    }

    public String getOnUpdate() {
        return onUpdate;
    }

    public void setOnUpdate(String onUpdate) {
        this.onUpdate = onUpdate;
    }

    public Object getNotes() {
        return notes;
    }

    public void setNotes(Object notes) {
        this.notes = notes;
    }

    public String getDl() {
        return dl;
    }

    public void setDl(String dl) {
        this.dl = dl;
    }

    public String getTotalHrs() {
        return totalHrs;
    }

    public void setTotalHrs(String totalHrs) {
        this.totalHrs = totalHrs;
    }

    public String getTotalPersons() {
        return totalPersons;
    }

    public void setTotalPersons(String totalPersons) {
        this.totalPersons = totalPersons;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public Object getExpertName() {
        return expertName;
    }

    public void setExpertName(Object expertName) {
        this.expertName = expertName;
    }

    public String getExpertRating() {
        return expertRating;
    }

    public void setExpertRating(String expertRating) {
        this.expertRating = expertRating;
    }
}
