package com.sukhizindagi.app.ui;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import android.app.Dialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.style.UnderlineSpan;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseApp;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.sukhizindagi.app.R;
import com.sukhizindagi.app.ui.helpers.Utils;
import com.sukhizindagi.app.ui.services.Result;
import com.sukhizindagi.app.ui.services.ServicesFactory;

import org.jetbrains.annotations.NotNull;
import org.json.JSONException;
import org.json.JSONObject;


public class SignUpActivity extends AppCompatActivity implements Result, View.OnClickListener {

    EditText emailEt, phoneEt, passwordEt, confirmPasswordEt, firstNameEt, lastnameEt;
    String email, phone, password, confirmPassword, firstname, lastname;
    TextView terms_tv;
    CardView signUpCard;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);
        initView();
    }

    String app_id = "";

    private void initView() {
        FirebaseApp.initializeApp(this);
        signUpCard = findViewById(R.id.signup_card);
        terms_tv = findViewById(R.id.terms_tv);
        terms_tv.setOnClickListener(this);
        String str = "Terms";
        SpannableString spannableString = new SpannableString(terms_tv.getText());
        spannableString.setSpan(new UnderlineSpan(), str.indexOf(str), spannableString.length(), Spanned.SPAN_INCLUSIVE_EXCLUSIVE);
        signUpCard.setOnClickListener(this);
        emailEt = findViewById(R.id.email_tv);
        phoneEt = findViewById(R.id.phone_tv);
        passwordEt = findViewById(R.id.password_tv);
        confirmPasswordEt = findViewById(R.id.confirm_password_tv);
        firstNameEt = findViewById(R.id.firstname_tv);
        lastnameEt = findViewById(R.id.secondname_tv);
        FirebaseInstanceId.getInstance().getInstanceId()
                .addOnCompleteListener(new OnCompleteListener<InstanceIdResult>() {
                    @Override
                    public void onComplete(@NonNull Task<InstanceIdResult> task) {
                        if (!task.isSuccessful()) {
                            Log.w("", "getInstanceId failed", task.getException());
                            return;
                        }

                        // Get new Instance ID token
                        app_id = task.getResult().getToken();
                    }
                });


    }

    @Override
    public void onSuccess(@NotNull String data, int requestCode) {
        try {
            JSONObject obj = new JSONObject(data);
            String userAsString = String.valueOf(obj);
            PreferenceManager.getDefaultSharedPreferences(this).edit().putString("user", userAsString).apply();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Toast.makeText(this, "SignUp Successful.", Toast.LENGTH_SHORT).show();
        Utils.dismissDialog(dialog);
        Intent intent = new Intent(SignUpActivity.this, PhoneVerificationActivity
                .class);
        intent.putExtra("mobile", phones);
        intent.putExtra("fromSignUp", "fromSignUp");
        startActivity(intent);
        finish();
    }

    @Override
    public void onFailure(@NotNull String cause, int requestCode) {
        Toast.makeText(this, "Failed SignUp. Kindly try again.", Toast.LENGTH_SHORT).show();
        Utils.dismissDialog(dialog);
    }

    Dialog dialog;

    private Boolean areInputsOkay() {
        if (TextUtils.isEmpty(emailEt.getText())) {
            emailEt.setError("Required");
            emailEt.requestFocus();
            return false;
        } else if (TextUtils.isEmpty(phoneEt.getText())) {
            phoneEt.setError("Required");
            phoneEt.requestFocus();
            return false;
        } else if (!Patterns.EMAIL_ADDRESS.matcher(emailEt.getText()).matches()) {
            emailEt.setError("Invalid Email");
            emailEt.requestFocus();
            return false;
        } else if (TextUtils.isEmpty(passwordEt.getText())) {
            passwordEt.setError("Required");
            passwordEt.requestFocus();
            return false;
        } else if (passwordEt.getText().length() < 6) {
            passwordEt.setError("Password is too short");
            passwordEt.requestFocus();
            return false;
        } else if (TextUtils.isEmpty(confirmPasswordEt.getText())) {
            confirmPasswordEt.setError("Required");
            confirmPasswordEt.requestFocus();
            return false;
        } else if (TextUtils.isEmpty(firstNameEt.getText())) {
            confirmPasswordEt.setError("Required");
            confirmPasswordEt.requestFocus();
            return false;
        } else if (TextUtils.isEmpty(lastnameEt.getText())) {
            confirmPasswordEt.setError("Required");
            confirmPasswordEt.requestFocus();
            return false;
        } else if (!passwordEt.getText().toString().equals(confirmPasswordEt.getText().toString())) {
            Toast.makeText(this, "Password does'nt match", Toast.LENGTH_SHORT).show();
        }
        firstname = firstNameEt.getText().toString();
        lastname = lastnameEt.getText().toString();
        phone = phoneEt.getText().toString();
        phones = phone.replaceFirst("0", "");
        email = emailEt.getText().toString();
        password = passwordEt.getText().toString();
        confirmPassword = confirmPasswordEt.getText().toString();
        return true;
    }

    String phones;

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.signup_card:

                if (areInputsOkay() && app_id != null) {
                    dialog = Utils.showLoadingDialog(this);
                    PreferenceManager.getDefaultSharedPreferences(this).edit().putString("user_email", email).putString("password", password).apply();
                    new ServicesFactory(this, 0).signUp(email, password, "1", app_id, phones, firstname, lastname);
                }
                break;
            case R.id.terms_tv:
                Uri uri = Uri.parse("https://sukhizindagi.com/terms-and-conditions/"); // missing 'http://' will cause crashed
                Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                startActivity(intent);

                break;
        }
    }
}
