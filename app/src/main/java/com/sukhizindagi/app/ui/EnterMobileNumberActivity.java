package com.sukhizindagi.app.ui;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.sukhizindagi.app.R;
import com.sukhizindagi.app.ui.helpers.Utils;
import com.sukhizindagi.app.ui.services.Result;
import com.sukhizindagi.app.ui.services.ServicesFactory;

import org.jetbrains.annotations.NotNull;

public class EnterMobileNumberActivity extends AppCompatActivity implements Result, View.OnClickListener {
    CardView resetPassword;
    private Dialog dialog;
    String phone;
    EditText phoneEt;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_enter_mobile_number);
        initView();
    }

    private void initView() {

        resetPassword = findViewById(R.id.reset_password_card);
        resetPassword.setOnClickListener(this);
        phoneEt = findViewById(R.id.mob_reset_tv);
    }

    @Override
    public void onSuccess(@NotNull String data, int requestCode) {
        Utils.dismissDialog(dialog);
        Intent intent = new Intent(EnterMobileNumberActivity.this, PhoneVerificationActivity.class);
        intent.putExtra("mobile", phones);
        startActivity(intent);

    }

    @Override
    public void onFailure(@NotNull String cause, int requestCode) {
        Toast.makeText(this, cause, Toast.LENGTH_SHORT).show();
        Utils.dismissDialog(dialog);

    }


    private Boolean areInputsOkay() {
        if (TextUtils.isEmpty(phoneEt.getText())) {
            phoneEt.setError("Required");
            phoneEt.requestFocus();
            return false;
        }
        phone = phoneEt.getText().toString();
        phones = phone.replaceFirst("0", "");
        return true;
    }

    String phones;

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.reset_password_card:
                if (areInputsOkay()) {
                    dialog = Utils.showLoadingDialog(this);
                    new ServicesFactory(this, 0).forgotPassword(phones);
                }
                break;

        }
    }

}
