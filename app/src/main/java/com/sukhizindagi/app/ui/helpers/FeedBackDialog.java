package com.sukhizindagi.app.ui.helpers;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.preference.PreferenceManager;
import android.text.TextUtils;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RatingBar;

import com.sukhizindagi.app.R;
import com.sukhizindagi.app.ui.JobRecordsActivity;
import com.sukhizindagi.app.ui.services.Result;
import com.sukhizindagi.app.ui.services.ServicesFactory;

import org.jetbrains.annotations.NotNull;


public class FeedBackDialog implements Result {
    EditText feedbackText;
    RatingBar ratingBar;
    Button feedbackBtn;
    String email, password;
    Dialog dialog;
    Activity activity;
    Dialog dialogtwo;

    public void showDialog(final Activity activity, final String jobId) {
        this.activity = activity;
        dialog = new Dialog(activity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.feedback_dialog);
        email = PreferenceManager.getDefaultSharedPreferences(activity).getString("user_email", "email");
        password = PreferenceManager.getDefaultSharedPreferences(activity).getString("password", "password");

        ratingBar = dialog.findViewById(R.id.rating);
        feedbackText = dialog.findViewById(R.id.getFeedbackEt);

        feedbackBtn = dialog.findViewById(R.id.fedbackcmplt);
        feedbackBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (areInputsOkay())
                    new ServicesFactory(FeedBackDialog.this, 0).feedBack(email, password, jobId, String.valueOf((int) ratingBar.getRating()), feedBackString);
                dialogtwo = Utils.showLoadingDialog(activity);
            }
        });

        dialog.show();

    }

    String feedBackString;

    private Boolean areInputsOkay() {
        if (TextUtils.isEmpty(feedbackText.getText())) {
            feedBackString = "";
            return true;
        } else {
            feedBackString = feedbackText.getText().toString();
            return true;
        }
    }

    @Override
    public void onSuccess(@NotNull String data, int requestCode) {
        Utils.dismissDialog(dialogtwo);
        dialog.dismiss();
        Intent intent = new Intent(activity, JobRecordsActivity.class);
        activity.startActivity(intent);
        activity.finish();
    }

    @Override
    public void onFailure(@NotNull String cause, int requestCode) {
        dialog.dismiss();
        Intent intent = new Intent(activity, JobRecordsActivity.class);
        activity.startActivity(intent);
        activity.finish();
    }
}
