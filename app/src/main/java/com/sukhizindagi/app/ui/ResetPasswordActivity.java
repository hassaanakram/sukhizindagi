package com.sukhizindagi.app.ui;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Patterns;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.sukhizindagi.app.R;
import com.sukhizindagi.app.ui.helpers.Utils;
import com.sukhizindagi.app.ui.services.Result;
import com.sukhizindagi.app.ui.services.ServicesFactory;

import org.jetbrains.annotations.NotNull;

public class ResetPasswordActivity extends AppCompatActivity implements View.OnClickListener, Result {
    CardView resetPassword;
    EditText passwordEt, confirmPasswordEt;
    String phone, password, confirmPassword, otp;
    private Dialog dialog;
    Intent intent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reset_password);
        initView();
    }

    private void initView() {
        intent = getIntent();
        if (intent != null) {
            phone = intent.getStringExtra("mobile");
            otp = intent.getStringExtra("otp");
        }
        resetPassword = findViewById(R.id.reset_password_card);
        resetPassword.setOnClickListener(this);
        passwordEt = findViewById(R.id.first_password_tv);
        confirmPasswordEt = findViewById(R.id.second_password_tv);
    }

    private Boolean areInputsOkay() {
        if (TextUtils.isEmpty(passwordEt.getText())) {
            passwordEt.setError("Required");
            passwordEt.requestFocus();
            return false;
        } else if (passwordEt.getText().length() < 6) {
            passwordEt.setError("Password is too short");
            passwordEt.requestFocus();
            return false;
        } else if (TextUtils.isEmpty(confirmPasswordEt.getText())) {
            confirmPasswordEt.setError("Required");
            confirmPasswordEt.requestFocus();
            return false;
        } else if (!passwordEt.getText().toString().equals(confirmPasswordEt.getText().toString())) {
            Toast.makeText(this, "Password does'nt match", Toast.LENGTH_SHORT).show();
        }
        password = passwordEt.getText().toString();
        confirmPassword = confirmPasswordEt.getText().toString();
        return true;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.reset_password_card:
                if (areInputsOkay()) {
                    dialog = Utils.showLoadingDialog(this);
                    new ServicesFactory(this, 0).resetPassword(phone, otp, password);
                }
                break;

        }
    }

    @Override
    public void onSuccess(@NotNull String data, int requestCode) {
        Utils.dismissDialog(dialog);
        Toast.makeText(this, "Password Reset Successful", Toast.LENGTH_SHORT).show();
        Intent intent = new Intent(this, SignInActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
        finish();

    }

    @Override
    public void onFailure(@NotNull String cause, int requestCode) {
        Utils.dismissDialog(dialog);
        Toast.makeText(this, "Failed to Reset Your Password. Kindly Try again.", Toast.LENGTH_SHORT).show();

    }
}
