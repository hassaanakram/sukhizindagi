package com.sukhizindagi.app.ui;

import android.content.Intent;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.style.UnderlineSpan;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.sukhizindagi.app.R;
import com.sukhizindagi.app.ui.helpers.Utils;
import com.sukhizindagi.app.ui.models.QuoteModel;

import java.util.Objects;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.ContextCompat;

public class QuoteDetailsActivity extends AppCompatActivity {

    Toolbar toolbar;
    TextView topBarText, tvTitle, tvNote, tvPrice, tvDate, tvStatus;
    MapView mapView;
    QuoteModel quoteModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_quote_details);
        quoteModel = (QuoteModel) getIntent().getSerializableExtra("QuoteModel");
        if(quoteModel == null) {
            Toast.makeText(this, "No Data Found", Toast.LENGTH_SHORT).show();
            finish();
        }
        initView(savedInstanceState);
    }

    private void initView(Bundle savedInstanceState) {
        toolbar = findViewById(R.id.toolbar_preview_booking);
        mapView = findViewById(R.id.mapView);
        tvTitle = findViewById(R.id.tvTitle);
        tvNote = findViewById(R.id.tvNote);
        tvPrice = findViewById(R.id.tvPrice);
        tvDate = findViewById(R.id.tvDate);
        tvStatus = findViewById(R.id.tvStatus);
        mapView.onCreate(savedInstanceState);
        tvDate.setText(quoteModel.getCreatedOn());
        tvTitle.setText(quoteModel.getCategoryName());
        tvNote.setText(quoteModel.getDetails() + "\n" + quoteModel.getAddress() + "\n" + quoteModel.getLatestNote());
        tvStatus.setText(quoteModel.getStatus());
        if(quoteModel.getPrice() == null || quoteModel.getPrice().isEmpty()) {
            tvPrice.setText("N/A");
        } else {
            tvPrice.setText("Pkr. " + quoteModel.getPrice());
        }

        mapView.onResume();
        mapView.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(GoogleMap googleMap) {
                if(quoteModel.getLatitude() != null && !quoteModel.getLatitude().isEmpty() && quoteModel.getLongitude() != null && !quoteModel.getLongitude().isEmpty()) {
                    LatLng position = new LatLng(Double.parseDouble(quoteModel.getLatitude()), Double.parseDouble(quoteModel.getLongitude()));
                    googleMap.addMarker(new MarkerOptions().position(position));
                    googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(position, 15));
                }
            }
        });
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(QuoteDetailsActivity.this, ExploreServicesActivity.class);
                startActivity(intent);
                finish();
            }
        });
        topBarText = findViewById(R.id.toolbar_title);
        topBarText.setText(quoteModel.getCategoryName());
        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
    }
}
