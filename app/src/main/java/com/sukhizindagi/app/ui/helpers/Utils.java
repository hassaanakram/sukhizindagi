package com.sukhizindagi.app.ui.helpers;

import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.view.Window;

import com.sukhizindagi.app.R;

public class Utils {
    public static void dismissDialog(Dialog dialog) {
        if(dialog != null && dialog.isShowing()) {
            dialog.dismiss();
        }
    }
    public static Dialog showLoadingDialog(Context context) {
        if(context != null) {
            Dialog dialog = new Dialog(context);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            if (dialog.getWindow() != null)
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.argb(50, 0, 0, 0)));
            dialog.setCancelable(true);
            dialog.setContentView(R.layout.dialog_loading);

            dialog.show();
            return dialog;
        }
        return null;
    }
}
