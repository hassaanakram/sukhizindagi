package com.sukhizindagi.app.ui;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.cardview.widget.CardView;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Patterns;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.sukhizindagi.app.R;
import com.sukhizindagi.app.ui.helpers.DialogHelper;

import java.text.SimpleDateFormat;
import java.util.Calendar;

import static com.sukhizindagi.app.ui.fragments.MapsFragment.MY_PERMISSIONS_REQUEST_LOCATION;

public class TaskDetailsActivity extends AppCompatActivity implements View.OnClickListener {

    CardView proceedNextCard, oneCardHour, twoCardHour, threeCardHour, fourCardHour, fiveCardHour, sixCardHour, sevenCardHour, eightCardHour,
            oneCardMaid, twoCardMaid, threeCardMaid, fourCardMaid, fiveCardMaid, timePickerCard, toDatePickerCard, fromDatePickerCard;
    TextView onehourTv, twohourTv, threehourTv, fourhourTv, fivehourTv, sixhourTv, sevenhourTv, eighthourTv,
            onehMaidTv, twohMaidTv, threehMaidTv, fourhMaidTv, fivehMaidTv, fromDatetv, time_picker_tv;
    String numberOfHoursString = "0", numberOfMaidsString = "0", date, time;
    Toolbar toolbar;
    Intent intent;
    int category;
    EditText job_title;

    private Boolean areInputsOkay() {
        if (TextUtils.isEmpty(job_title.getText())) {
            job_title.setError("Required");
            job_title.requestFocus();
            return false;
        } else if ((time_picker_tv.getText().toString().equals("Select Time"))) {
            time_picker_tv.setError("Required");
            time_picker_tv.requestFocus();
            return false;
        } else if ((fromDatetv.getText().toString().equals("Select Date"))) {
            fromDatetv.setError("Required");
            fromDatetv.requestFocus();
            return false;
        }
        jobTitle = job_title.getText().toString();
        date = fromDatetv.getText().toString();
        time = time_picker_tv.getText().toString();
        return true;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_task_details);
        initView();
    }

    private void initView() {
        intent = getIntent();
        if (intent != null) {
            category = intent.getIntExtra("category_name", 0);
        }
        toolbar = findViewById(R.id.toolbar_task_details);
        setSupportActionBar(toolbar);
        job_title = findViewById(R.id.specify_details_tv);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        proceedNextCard = findViewById(R.id.proceed_next_card);
        proceedNextCard.setOnClickListener(this);

        //hours card listeners
        oneCardHour = findViewById(R.id.one_card_hour);
        oneCardHour.setOnClickListener(this);
        twoCardHour = findViewById(R.id.two_card_hour);
        twoCardHour.setOnClickListener(this);
        threeCardHour = findViewById(R.id.three_card_hour);
        threeCardHour.setOnClickListener(this);
        fourCardHour = findViewById(R.id.four_card_hour);
        fourCardHour.setOnClickListener(this);
        fiveCardHour = findViewById(R.id.five_card_hour);
        fiveCardHour.setOnClickListener(this);
        sixCardHour = findViewById(R.id.six_card_hour);
        sixCardHour.setOnClickListener(this);
        sevenCardHour = findViewById(R.id.seven_card_hour);
        sevenCardHour.setOnClickListener(this);
        eightCardHour = findViewById(R.id.eight_card_hour);
        eightCardHour.setOnClickListener(this);
        //textviews four hours
        onehourTv = findViewById(R.id.one_card_hour_tv);
        twohourTv = findViewById(R.id.two_card_hour_tv);
        threehourTv = findViewById(R.id.three_card_hour_tv);
        fourhourTv = findViewById(R.id.four_card_hour_tv);
        fivehourTv = findViewById(R.id.five_card_hour_tv);
        sixhourTv = findViewById(R.id.six_card_hour_tv);
        sevenhourTv = findViewById(R.id.seven_card_hour_tv);
        eighthourTv = findViewById(R.id.eight_card_hour_tv);
        //maid cards listeners
        oneCardMaid = findViewById(R.id.one_card_maid);
        oneCardMaid.setOnClickListener(this);
        twoCardMaid = findViewById(R.id.two_card_maid);
        twoCardMaid.setOnClickListener(this);
        threeCardMaid = findViewById(R.id.three_card_maid);
        threeCardMaid.setOnClickListener(this);
        fourCardMaid = findViewById(R.id.four_card_maid);
        fourCardMaid.setOnClickListener(this);
        fiveCardMaid = findViewById(R.id.five_card_maid);
        fiveCardMaid.setOnClickListener(this);
        //maid textviews listeners
        onehMaidTv = findViewById(R.id.one_card_maid_tv);
        twohMaidTv = findViewById(R.id.two_card_maid_tv);
        threehMaidTv = findViewById(R.id.three_card_maid_tv);
        fourhMaidTv = findViewById(R.id.four_card_maid_tv);
        fivehMaidTv = findViewById(R.id.five_card_maid_tv);

        //time picker date picker
        timePickerCard = findViewById(R.id.prefered_time_card);
        timePickerCard.setOnClickListener(this);
        time_picker_tv = findViewById(R.id.prefered_time_tv);
        fromDatePickerCard = findViewById(R.id.from_select_date_card);
        fromDatePickerCard.setOnClickListener(this);
        fromDatetv = findViewById(R.id.date_tv);
    }

    CardView selectedCard, selectedMaidHour;
    TextView selectedText, selectedMaidTv;

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.proceed_next_card:
                if (isRightTime)
                    if (numberOfMaidsString.contains("0") || numberOfHoursString.contains("0")) {
                        Toast.makeText(this, "Kindly select proper data", Toast.LENGTH_SHORT).show();
                        return;
                    } else {
                        if (areInputsOkay()) {
                            if (!checkLocationPermission()) {
                                return;
                            }
                            Intent intent = new Intent(this, ChoseLocationActivity.class);
                            intent.putExtra("date", date);
                            intent.putExtra("time", time);
                            intent.putExtra("maids", numberOfMaidsString);
                            intent.putExtra("hours", numberOfHoursString);
                            intent.putExtra("job_title", jobTitle);
                            intent.putExtra("category", category);
                            startActivity(intent);
                        }
                    }
                else {
                    Toast.makeText(TaskDetailsActivity.this, "Kindly select time between 9AM to 5PM.", Toast.LENGTH_LONG).show();

                }
                break;
            //card four hours listeners
            case R.id.one_card_hour:
                if (selectedCard != null) {
                    selectedCard.setCardBackgroundColor(Color.parseColor("#ffffff"));
                    selectedText.setTextColor(Color.parseColor("#000000"));
                }
                oneCardHour.setCardBackgroundColor(Color.parseColor("#008598"));
                onehourTv.setTextColor(Color.parseColor("#ffffff"));
                numberOfHoursString = "1";
                selectedCard = oneCardHour;
                selectedText = onehourTv;
                break;
            case R.id.two_card_hour:
                if (selectedCard != null) {
                    selectedCard.setCardBackgroundColor(Color.parseColor("#ffffff"));
                    selectedText.setTextColor(Color.parseColor("#000000"));
                }
                twoCardHour.setCardBackgroundColor(Color.parseColor("#008598"));
                twohourTv.setTextColor(Color.parseColor("#ffffff"));
                selectedCard = twoCardHour;
                numberOfHoursString = "2";

                selectedText = twohourTv;
                break;
            case R.id.three_card_hour:
                if (selectedCard != null) {
                    selectedCard.setCardBackgroundColor(Color.parseColor("#ffffff"));
                    selectedText.setTextColor(Color.parseColor("#000000"));
                }
                threeCardHour.setCardBackgroundColor(Color.parseColor("#008598"));
                threehourTv.setTextColor(Color.parseColor("#ffffff"));
                selectedCard = threeCardHour;
                numberOfHoursString = "3";

                selectedText = threehourTv;
                break;
            case R.id.four_card_hour:
                if (selectedCard != null) {
                    selectedCard.setCardBackgroundColor(Color.parseColor("#ffffff"));
                    selectedText.setTextColor(Color.parseColor("#000000"));
                }
                fourCardHour.setCardBackgroundColor(Color.parseColor("#008598"));
                fourhourTv.setTextColor(Color.parseColor("#ffffff"));
                selectedCard = fourCardHour;
                numberOfHoursString = "4";

                selectedText = fourhourTv;
                break;
            case R.id.five_card_hour:
                if (selectedCard != null) {
                    selectedCard.setCardBackgroundColor(Color.parseColor("#ffffff"));
                    selectedText.setTextColor(Color.parseColor("#000000"));
                }
                fiveCardHour.setCardBackgroundColor(Color.parseColor("#008598"));
                fivehourTv.setTextColor(Color.parseColor("#ffffff"));
                selectedCard = fiveCardHour;
                numberOfHoursString = "5";

                selectedText = fivehourTv;
                break;
            case R.id.six_card_hour:
                if (selectedCard != null) {
                    selectedCard.setCardBackgroundColor(Color.parseColor("#ffffff"));
                    selectedText.setTextColor(Color.parseColor("#000000"));
                }
                sixCardHour.setCardBackgroundColor(Color.parseColor("#008598"));
                sixhourTv.setTextColor(Color.parseColor("#ffffff"));
                selectedCard = sixCardHour;
                selectedText = sixhourTv;
                numberOfHoursString = "6";

                break;
            case R.id.seven_card_hour:
                if (selectedCard != null) {
                    selectedCard.setCardBackgroundColor(Color.parseColor("#ffffff"));
                    selectedText.setTextColor(Color.parseColor("#000000"));
                }
                sevenCardHour.setCardBackgroundColor(Color.parseColor("#008598"));
                sevenhourTv.setTextColor(Color.parseColor("#ffffff"));
                selectedCard = sevenCardHour;
                numberOfHoursString = "7";

                selectedText = sevenhourTv;
                break;
            case R.id.eight_card_hour:
                if (selectedCard != null) {
                    selectedCard.setCardBackgroundColor(Color.parseColor("#ffffff"));
                    selectedText.setTextColor(Color.parseColor("#000000"));
                }
                eightCardHour.setCardBackgroundColor(Color.parseColor("#008598"));
                eighthourTv.setTextColor(Color.parseColor("#ffffff"));
                selectedCard = eightCardHour;
                selectedText = eighthourTv;
                numberOfHoursString = "8";

                break;
            //card for maids listeners
            case R.id.one_card_maid:
                if (selectedMaidHour != null) {
                    selectedMaidHour.setCardBackgroundColor(Color.parseColor("#ffffff"));
                    selectedMaidTv.setTextColor(Color.parseColor("#000000"));
                }
                oneCardMaid.setCardBackgroundColor(Color.parseColor("#008598"));
                onehMaidTv.setTextColor(Color.parseColor("#ffffff"));
                selectedMaidHour = oneCardMaid;
                selectedMaidTv = onehMaidTv;
                numberOfMaidsString = "1";

                break;
            case R.id.two_card_maid:
                if (selectedMaidHour != null) {
                    selectedMaidHour.setCardBackgroundColor(Color.parseColor("#ffffff"));
                    selectedMaidTv.setTextColor(Color.parseColor("#000000"));
                }
                twoCardMaid.setCardBackgroundColor(Color.parseColor("#008598"));
                twohMaidTv.setTextColor(Color.parseColor("#ffffff"));
                selectedMaidHour = twoCardMaid;
                selectedMaidTv = twohMaidTv;
                numberOfMaidsString = "2";

                break;
            case R.id.three_card_maid:
                if (selectedMaidHour != null) {
                    selectedMaidHour.setCardBackgroundColor(Color.parseColor("#ffffff"));
                    selectedMaidTv.setTextColor(Color.parseColor("#000000"));
                }
                threeCardMaid.setCardBackgroundColor(Color.parseColor("#008598"));
                threehMaidTv.setTextColor(Color.parseColor("#ffffff"));
                selectedMaidHour = threeCardMaid;
                selectedMaidTv = threehMaidTv;
                numberOfMaidsString = "3";

                break;
            case R.id.four_card_maid:
                if (selectedMaidHour != null) {
                    selectedMaidHour.setCardBackgroundColor(Color.parseColor("#ffffff"));
                    selectedMaidTv.setTextColor(Color.parseColor("#000000"));
                }
                fourCardMaid.setCardBackgroundColor(Color.parseColor("#008598"));
                fourhMaidTv.setTextColor(Color.parseColor("#ffffff"));
                selectedMaidHour = fourCardMaid;
                selectedMaidTv = fourhMaidTv;
                numberOfMaidsString = "4";
                break;
            case R.id.five_card_maid:
                if (selectedMaidHour != null) {
                    selectedMaidHour.setCardBackgroundColor(Color.parseColor("#ffffff"));
                    selectedMaidTv.setTextColor(Color.parseColor("#000000"));
                }
                fiveCardMaid.setCardBackgroundColor(Color.parseColor("#008598"));
                fivehMaidTv.setTextColor(Color.parseColor("#ffffff"));
                selectedMaidHour = fiveCardMaid;
                selectedMaidTv = fivehMaidTv;
                numberOfMaidsString = "5";
                break;

            case R.id.from_select_date_card:
                final Calendar c = Calendar.getInstance();
                int mYear = c.get(Calendar.YEAR);
                int mMonth = c.get(Calendar.MONTH);
                int mDay = c.get(Calendar.DAY_OF_MONTH);


                DatePickerDialog datePickerDialog = new DatePickerDialog(this,
                        new DatePickerDialog.OnDateSetListener() {

                            @Override
                            public void onDateSet(DatePicker view, int year,
                                                  int monthOfYear, int dayOfMonth) {

                                fromDatetv.setText(year + "/" + (monthOfYear + 1) + "/" + dayOfMonth);

                            }
                        }, mYear, mMonth, mDay);
                datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
                datePickerDialog.show();
                break;
            case R.id.prefered_time_card:
                selectTIme();
                break;


        }
    }

    String jobTitle;

    boolean isRightTime = false;

    private void selectTIme() {
        Calendar mcurrentTime = Calendar.getInstance();
        int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
        int minute = mcurrentTime.get(Calendar.MINUTE);
        TimePickerDialog mTimePicker;
        mTimePicker = new TimePickerDialog(TaskDetailsActivity.this, new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                if (selectedHour > 17 || selectedHour < 9) {
                    isRightTime = false;
                    Toast.makeText(TaskDetailsActivity.this, "Kindly select time between 9AM to 5PM.", Toast.LENGTH_LONG).show();
                } else {
                    isRightTime = true;
                    Calendar selectedTime = Calendar.getInstance();
                    selectedTime.set(selectedTime.get(Calendar.YEAR), selectedTime.get(Calendar.MONTH), selectedTime.get(Calendar.DAY_OF_MONTH), selectedHour, selectedMinute);
                    SimpleDateFormat dateFormatter = new SimpleDateFormat(
                            "hh:mm a");
//                if (selectedTime.getTime().after())
                    String time = dateFormatter.format(selectedTime.getTime());
                    time_picker_tv.setText(time);
                }
            }
        }, hour, minute, false);
        mTimePicker.setTitle("Select Time");

        mTimePicker.show();

    }

    public boolean checkLocationPermission() {
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED && ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            DialogHelper.showPermissionDialog(this, new DialogHelper.DialogButtonsListener<Boolean>() {
                @Override
                public void positiveButtonClick(Boolean aBoolean) {
                    ActivityCompat.requestPermissions(TaskDetailsActivity.this,
                            new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION},
                            MY_PERMISSIONS_REQUEST_LOCATION);
                }

                @Override
                public void negativeButtonClick(Boolean aBoolean) {

                }

                @Override
                public void canceled(Boolean aBoolean) {

                }
            });
            return false;
        } else {
            return true;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_LOCATION: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    // permission was granted, yay! Do the
                    // location-related task you need to do.
                    if (ContextCompat.checkSelfPermission(this,
                            Manifest.permission.ACCESS_FINE_LOCATION)
                            == PackageManager.PERMISSION_GRANTED) {
                        Intent intent = new Intent(this, ChoseLocationActivity.class);
                        intent.putExtra("date", date);
                        intent.putExtra("time", time);
                        intent.putExtra("maids", numberOfMaidsString);
                        intent.putExtra("hours", numberOfHoursString);
                        intent.putExtra("job_title", jobTitle);
                        intent.putExtra("category", category);
                        startActivity(intent);
                        //Request location updates:
                    }

                } else {
                    checkLocationPermission();

                }
                return;
            }

        }
    }
}
