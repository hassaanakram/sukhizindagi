package com.sukhizindagi.app.ui;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.cardview.widget.CardView;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.text.TextUtils;
import android.util.Patterns;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.sukhizindagi.app.R;
import com.sukhizindagi.app.ui.helpers.Utils;
import com.sukhizindagi.app.ui.models.User;
import com.sukhizindagi.app.ui.services.Result;
import com.sukhizindagi.app.ui.services.ServicesFactory;

import org.jetbrains.annotations.NotNull;
import org.json.JSONException;
import org.json.JSONObject;

public class CheckUserLoginSignupActivity extends AppCompatActivity implements View.OnClickListener, Result {
    CardView alreadyCustomerLoginCard, alreadyCustomerProceedCard, newCustomerCard;
    Toolbar toolbar;
    TextView signUp;
    String date, time, maids, hours, job_title, email, password, address, adresFromUser,longitude,latitude;
    EditText emailEt, phoneEt, passwordEt, confirmPasswordEt, firstNameEt, lastnameEt;
    String emails, phone, passwords, confirmPassword, firstname, lastname;
    int category;
    Intent intent;
    User userObj = null;
    Gson gson = new Gson();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_check_user_login_signup);
        try {
            String userString = PreferenceManager.getDefaultSharedPreferences(this).getString("user", null);
            JSONObject jsonObject = new JSONObject(userString);
            userObj = gson.fromJson(jsonObject.getString("user"), User.class);
        } catch (Exception e) {
            e.printStackTrace();
        }
        initView();
    }

    private Boolean areInputsOkay() {
        if (TextUtils.isEmpty(emailEt.getText())) {
            emailEt.setError("Required");
            emailEt.requestFocus();
            return false;
        } else if (TextUtils.isEmpty(phoneEt.getText())) {
            phoneEt.setError("Required");
            phoneEt.requestFocus();
            return false;
        } else if (!Patterns.EMAIL_ADDRESS.matcher(emailEt.getText()).matches()) {
            emailEt.setError("Invalid Email");
            emailEt.requestFocus();
            return false;
        } else if (TextUtils.isEmpty(passwordEt.getText())) {
            passwordEt.setError("Required");
            passwordEt.requestFocus();
            return false;
        } else if (passwordEt.getText().length() < 6) {
            passwordEt.setError("Password is too short");
            passwordEt.requestFocus();
            return false;
        } else if (TextUtils.isEmpty(confirmPasswordEt.getText())) {
            confirmPasswordEt.setError("Required");
            confirmPasswordEt.requestFocus();
            return false;
        } else if (TextUtils.isEmpty(firstNameEt.getText())) {
            confirmPasswordEt.setError("Required");
            confirmPasswordEt.requestFocus();
            return false;
        } else if (TextUtils.isEmpty(lastnameEt.getText())) {
            confirmPasswordEt.setError("Required");
            confirmPasswordEt.requestFocus();
            return false;
        } else if (!passwordEt.getText().toString().equals(confirmPasswordEt.getText().toString())) {
            Toast.makeText(this, "Password does'nt match", Toast.LENGTH_SHORT).show();
        }
        firstname = firstNameEt.getText().toString();
        lastname = lastnameEt.getText().toString();
        phone = phoneEt.getText().toString();
        phones = phone.replaceFirst("0", "");
        email = emailEt.getText().toString();
        password = passwordEt.getText().toString();
        confirmPassword = confirmPasswordEt.getText().toString();
        return true;
    }

    String phones;

    private void initView() {
        intent = getIntent();
        if (intent != null) {
            category = intent.getIntExtra("category", 0);
            date = intent.getStringExtra("date");
            time = intent.getStringExtra("time");
            maids = intent.getStringExtra("maids");
            hours = intent.getStringExtra("hours");
            email = intent.getStringExtra("email");
            password = intent.getStringExtra("password");
            address = intent.getStringExtra("address");
            job_title = intent.getStringExtra("job_title");
            adresFromUser = intent.getStringExtra("addressOfUserFromMap");
            latitude = intent.getStringExtra("latitude");
            longitude = intent.getStringExtra("longitude");
        }
        toolbar = findViewById(R.id.toolbar_check_user);
        setSupportActionBar(toolbar);

        signUp = findViewById(R.id.signup_below_tv);
        signUp.setOnClickListener(this);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(CheckUserLoginSignupActivity.this, ChoseLocationActivity.class);
                startActivity(intent);
                finish();
            }
        });
        alreadyCustomerLoginCard = findViewById(R.id.already_customer_login_card);
        alreadyCustomerLoginCard.setOnClickListener(this);
        alreadyCustomerProceedCard = findViewById(R.id.already_customer_proceed_card);
        alreadyCustomerProceedCard.setOnClickListener(this);
        emailEt = findViewById(R.id.email_tv);
        phoneEt = findViewById(R.id.phone_tv);
        passwordEt = findViewById(R.id.password_tv);
        confirmPasswordEt = findViewById(R.id.confirm_password_tv);
        firstNameEt = findViewById(R.id.firstname_tv);
        lastnameEt = findViewById(R.id.secondname_tv);
//        newCustomerCard = findViewById(R.id.new_customer_signUp_card);
//        newCustomerCard.setOnClickListener(this);
    }

    Dialog dialog;

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.already_customer_login_card:
                Intent intentL = new Intent(this, SignInActivity.class);
                intentL.putExtra("startedResult", true);
                intentL.putExtra("date", date);
                intentL.putExtra("time", time);
                intentL.putExtra("maids", maids);
                intentL.putExtra("hours", hours);
                intentL.putExtra("job_title", job_title);
                intentL.putExtra("email", email);
                intentL.putExtra("password", password);
                intentL.putExtra("addressOfUserFromMap", adresFromUser);
                intentL.putExtra("category", category);
                intentL.putExtra("longitude", longitude);
                intentL.putExtra("latitude", latitude);
                startActivityForResult(intentL, 0);

                break;
            case R.id.already_customer_proceed_card:
                if (areInputsOkay()) {
                    dialog = Utils.showLoadingDialog(this);
                    PreferenceManager.getDefaultSharedPreferences(this).edit().putString("user_email", email).putString("password", password).apply();
                    new ServicesFactory(this, 0).signUp(email, password, "1", "1", phones, firstname, lastname);
                    break;
                }
                if (userObj != null) {
                    Intent intent = new Intent(this, PreviewBookingActivity.class);
                    intent.putExtra("date", date);
                    intent.putExtra("time", time);
                    intent.putExtra("maids", maids);
                    intent.putExtra("hours", hours);
                    intent.putExtra("job_title", job_title);
                    intent.putExtra("email", email);
                    intent.putExtra("password", password);
                    intent.putExtra("addressOfUserFromMap", adresFromUser);
                    intent.putExtra("mobile",phones);
                    intent.putExtra("category", category);
                    intent.putExtra("longitude", longitude);
                    intent.putExtra("latitude", latitude);
                    startActivity(intent);
                } else {
                    Toast.makeText(this, "Please Sign Up or Login to continue", Toast.LENGTH_SHORT).show();
                }
                break;

        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 0 && resultCode == Activity.RESULT_OK) {
            Intent intentPreview = new Intent(this, PreviewBookingActivity.class);
            intentPreview.putExtra("date", date);
            intentPreview.putExtra("time", time);
            intentPreview.putExtra("maids", maids);
            intentPreview.putExtra("hours", hours);
            intentPreview.putExtra("job_title", job_title);
            intentPreview.putExtra("email", email);
            intentPreview.putExtra("password", password);
            intentPreview.putExtra("addressOfUserFromMap", adresFromUser);
            intentPreview.putExtra("category", category);
            intentPreview.putExtra("latitude", latitude);
            intentPreview.putExtra("longitude", longitude);
            startActivity(intentPreview);
        } else if (requestCode == 1 && resultCode == Activity.RESULT_OK) {
            Intent intentPreview = new Intent(this, PreviewBookingActivity.class);
            intentPreview.putExtra("date", date);
            intentPreview.putExtra("time", time);
            intentPreview.putExtra("maids", maids);
            intentPreview.putExtra("hours", hours);
            intentPreview.putExtra("job_title", job_title);
            intentPreview.putExtra("email", email);
            intentPreview.putExtra("password", password);
            intentPreview.putExtra("addressOfUserFromMap", adresFromUser);
            intentPreview.putExtra("category", category);
            startActivity(intentPreview);
        }
    }

    @Override
    public void onSuccess(@NotNull String data, int requestCode) {
        Utils.dismissDialog(dialog);
        try {
            JSONObject obj = new JSONObject(data);
            String userAsString = String.valueOf(obj);
            PreferenceManager.getDefaultSharedPreferences(this).edit().putString("user", userAsString).apply();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Intent intentForResult = new Intent(this, PhoneVerificationActivity.class);
        intentForResult.putExtra("setForResult", true);
        intentForResult.putExtra("date", date);
        intentForResult.putExtra("time", time);
        intentForResult.putExtra("maids", maids);
        intentForResult.putExtra("hours", hours);
        intentForResult.putExtra("job_title", job_title);
        intentForResult.putExtra("email", email);
        intentForResult.putExtra("password", password);
        intentForResult.putExtra("mobile",phones);
        intentForResult.putExtra("addressOfUserFromMap", adresFromUser);
        intentForResult.putExtra("category", category);
        intentForResult.putExtra("longitude", longitude);
        intentForResult.putExtra("latitude", latitude);
        startActivityForResult(intentForResult, 1);
    }

    @Override
    public void onFailure(@NotNull String cause, int requestCode) {

    }
}
