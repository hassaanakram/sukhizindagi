package com.sukhizindagi.app.ui;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import com.sukhizindagi.app.R;

public class AlreadyUserLoginActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_already_user_login);
    }
}
