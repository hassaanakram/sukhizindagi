package com.sukhizindagi.app.ui.adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.sukhizindagi.app.R;
import com.sukhizindagi.app.ui.JobsDetailActivity;
import com.sukhizindagi.app.ui.models.JobsModel;

import java.util.ArrayList;
import java.util.List;

public class OngoingJobsAdapter extends ArrayAdapter<JobsModel> implements View.OnClickListener {

    private List<JobsModel> dataSet;
    Context mContext;

    // View lookup cache
    private static class ViewHolder {
        TextView serviceNmaeTv;
        TextView dateTv;
        TextView timeTv;
        ImageView info;
    }

    public OngoingJobsAdapter(List<JobsModel> data, Context context) {
        super(context, R.layout.ongoingjobs_adapter, data);
        this.dataSet = data;
        this.mContext = context;

    }

    @Override
    public void onClick(View v) {

        int position = (Integer) v.getTag();
        Object object = getItem(position);
        JobsModel dataModel = (JobsModel) object;
    }

    private int lastPosition = -1;

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // Get the data item for this position
        JobsModel dataModel = getItem(position);
        // Check if an existing view is being reused, otherwise inflate the view
        ViewHolder viewHolder; // view lookup cache stored in tag

        final View result;

        if (convertView == null) {

            viewHolder = new ViewHolder();
            LayoutInflater inflater = LayoutInflater.from(getContext());
            convertView = inflater.inflate(R.layout.ongoingjobs_adapter, parent, false);
            viewHolder.serviceNmaeTv = convertView.findViewById(R.id.maid_services_tv);
            viewHolder.dateTv = convertView.findViewById(R.id.maid_services_date_tv);
            viewHolder.timeTv = convertView.findViewById(R.id.maid_services_time_tv);
            result = convertView;

            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
            result = convertView;
        }

        Animation animation = AnimationUtils.loadAnimation(mContext, (position > lastPosition) ? R.anim.up_from_bottom : R.anim.down_from_top);
        result.startAnimation(animation);
        lastPosition = position;

        viewHolder.timeTv.setText(dataModel.getTiming());
        viewHolder.dateTv.setText(dataModel.getUserDate());
        viewHolder.serviceNmaeTv.setText(dataModel.getJobTitle());
//        viewHolder.info.setOnClickListener(this);
//        viewHolder.info.setTag(position);
        // Return the completed view to render on screen
        return convertView;
    }
}