package com.sukhizindagi.app.ui.services

interface Result {

    fun onSuccess(data: String, requestCode: Int)
    fun onFailure(cause: String, requestCode: Int)
}