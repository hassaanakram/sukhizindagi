package com.sukhizindagi.app.ui;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.FragmentTransaction;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.sukhizindagi.app.R;
import com.sukhizindagi.app.ui.fragments.MapsFragment;
import com.sukhizindagi.app.ui.helpers.DialogHelper;
import com.sukhizindagi.app.ui.helpers.Utils;
import com.sukhizindagi.app.ui.models.User;
import com.sukhizindagi.app.ui.services.Result;

import org.jetbrains.annotations.NotNull;
import org.json.JSONObject;

public class ChoseLocationActivity extends AppCompatActivity implements View.OnClickListener, Result {
    MapsFragment mapFragment = new MapsFragment();
    CardView proceedCard;
    TextView adressTv;
    EditText adressFromUser;
    Toolbar toolbar;
    Intent intent;
    int category;
    String date, time, maids, hours, job_title, email, password;
    String addressOfUserFromMap;
    User userObj = null;
    Gson gson = new Gson();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chose_location);
        email = PreferenceManager.getDefaultSharedPreferences(this).getString("user_email", "email");
        password = PreferenceManager.getDefaultSharedPreferences(this).getString("password", "password");
        initView();
        try {
            String userString = PreferenceManager.getDefaultSharedPreferences(this).getString("user", null);
            JSONObject jsonObject = new JSONObject(userString);
            userObj = gson.fromJson(jsonObject.getString("user"), User.class);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void initView() {
        intent = getIntent();
        if (intent != null) {
            category = intent.getIntExtra("category", 0);
            date = intent.getStringExtra("date");
            time = intent.getStringExtra("time");
            maids = intent.getStringExtra("maids");
            hours = intent.getStringExtra("hours");
            job_title = intent.getStringExtra("job_title");
            lati = intent.getStringExtra("latitude");
            longi = intent.getStringExtra("longitude");
        }

        final FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.add(R.id.chose_location_frame, mapFragment);

        transaction.replace(R.id.chose_location_frame, mapFragment);
        transaction.commit();
        adressTv = findViewById(R.id.address_from_frag);
        proceedCard = findViewById(R.id.proceed_next_card);
        proceedCard.setOnClickListener(this);
        adressFromUser = findViewById(R.id.manual_adress);
        toolbar = findViewById(R.id.toolbar_chose_location);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    boolean goNext;
    String lati, longi;

    public void getAddressFromFragment(String address, String city, String state, String country, Boolean goFurther, double lat, double lon) {
        goNext = goFurther;
        addressOfUserFromMap = address;
        adressTv.setText(address);
        lati = String.valueOf(lat);
        longi = String.valueOf(lon);

    }

    String adresFromUser;
    Dialog dialog;

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.proceed_next_card:
                if (goNext) {
                    if (!TextUtils.isEmpty(adressFromUser.getText())) {
                        adresFromUser = adressFromUser.getText().toString();
                        if (userObj != null) {
                            intent = new Intent(ChoseLocationActivity.this, PreviewBookingActivity.class);
                        } else {
                            intent = new Intent(ChoseLocationActivity.this, CheckUserLoginSignupActivity.class);
                        }
                        intent.putExtra("date", date);
                        intent.putExtra("time", time);
                        intent.putExtra("maids", maids);
                        intent.putExtra("hours", hours);
                        intent.putExtra("job_title", job_title);
                        intent.putExtra("email", email);
                        intent.putExtra("password", password);
                        intent.putExtra("addressOfUserFromMap", adresFromUser);
                        intent.putExtra("category", category);
                        intent.putExtra("longitude",longi);
                        intent.putExtra("latitude",lati);
                        startActivity(intent);
                    } else if (userObj != null) {
                        intent = new Intent(ChoseLocationActivity.this, PreviewBookingActivity.class);
                    } else {
                        intent = new Intent(ChoseLocationActivity.this, CheckUserLoginSignupActivity.class);
                    }
                    intent.putExtra("date", date);
                    intent.putExtra("time", time);
                    intent.putExtra("maids", maids);
                    intent.putExtra("hours", hours);
                    intent.putExtra("job_title", job_title);
                    intent.putExtra("email", email);
                    intent.putExtra("password", password);
                    intent.putExtra("addressOfUserFromMap", addressOfUserFromMap);
                    intent.putExtra("category", category);
                    intent.putExtra("longitude",longi);
                    intent.putExtra("latitude",lati);
                    startActivity(intent);
                } else {
                    Toast.makeText(this, "We are not serving in this area!", Toast.LENGTH_SHORT).show();
                }
                break;
        }
    }

    @Override
    public void onSuccess(@NotNull String data, int requestCode) {
        Utils.dismissDialog(dialog);
        Toast.makeText(this, data, Toast.LENGTH_SHORT).show();
        DialogHelper alert = new DialogHelper();
        alert.showDialog(ChoseLocationActivity.this);
    }

    @Override
    public void onFailure(@NotNull String cause, int requestCode) {
        Toast.makeText(this, cause, Toast.LENGTH_SHORT).show();
        Utils.dismissDialog(dialog);
    }
}
