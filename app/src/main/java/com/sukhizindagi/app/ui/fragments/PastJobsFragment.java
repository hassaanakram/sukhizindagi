package com.sukhizindagi.app.ui.fragments;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.google.gson.Gson;
import com.sukhizindagi.app.R;
import com.sukhizindagi.app.ui.JobsDetailActivity;
import com.sukhizindagi.app.ui.adapters.OngoingJobsAdapter;
import com.sukhizindagi.app.ui.helpers.Utils;
import com.sukhizindagi.app.ui.models.JobsModel;
import com.sukhizindagi.app.ui.models.User;
import com.sukhizindagi.app.ui.services.Result;
import com.sukhizindagi.app.ui.services.ServicesFactory;

import org.jetbrains.annotations.NotNull;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


public class PastJobsFragment extends Fragment implements Result {
    ImageView forwardIv;
    List<JobsModel> jobsModelsArray = new ArrayList<JobsModel>();
    ListView ongoingJobsList;

    private static OngoingJobsAdapter adapter;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_ongoing_jobs, container, false);

        initView(view);
        dialog = Utils.showLoadingDialog(getActivity());
        new ServicesFactory(this, 0).pastJobs(userObj.getEmail(), password);

        return view;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    private String email, password;
    Dialog dialog;
    User userObj;
    Gson gson = new Gson();

    private void initView(View view) {
        ongoingJobsList = view.findViewById(R.id.list_ongoing_jobs);
        try {
            String userString = PreferenceManager.getDefaultSharedPreferences(getContext()).getString("user", null);
            JSONObject jsonObject = new JSONObject(userString);
            userObj = gson.fromJson(jsonObject.getString("user"), User.class);
        } catch (Exception e) {
            e.printStackTrace();
        }
        password = PreferenceManager.getDefaultSharedPreferences(getActivity()).getString("password", "password");
    }


    List<JobsModel> pastJobsArray = new ArrayList<JobsModel>();

    @Override
    public void onSuccess(@NotNull String data, int requestCode) {
        Utils.dismissDialog(dialog);
        Gson gson = new Gson();
        try {
            JSONObject obj = new JSONObject(data);
            JSONArray jobs = obj.getJSONArray("Jobs");
            String jobsString = String.valueOf(jobs);
            jobsModelsArray = Arrays.asList(gson.fromJson(jobsString, JobsModel[].class));
            for (int i = 0; i < jobsModelsArray.size(); i++) {
                JobsModel jobsModel = new JobsModel();
                jobsModel = jobsModelsArray.get(i);
                pastJobsArray.add(jobsModel);

            }
            if (pastJobsArray != null) {
                adapter = new OngoingJobsAdapter(pastJobsArray, getActivity());
                ongoingJobsList.setAdapter(adapter);
                ongoingJobsList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        JobsModel jobsModel = pastJobsArray.get(position);
                        Intent intent = new Intent(getContext(), JobsDetailActivity.class);
                        intent.putExtra("obj", jobsModel);
                        startActivity(intent);
                    }
                });
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onFailure(@NotNull String cause, int requestCode) {

        Utils.dismissDialog(dialog);
    }

}
