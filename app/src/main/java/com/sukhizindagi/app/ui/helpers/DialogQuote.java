package com.sukhizindagi.app.ui.helpers;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.view.View;
import android.widget.ImageView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.sukhizindagi.app.R;
import com.sukhizindagi.app.ui.ExploreServicesActivity;

public class DialogQuote {

    public void showDialog(final Activity activity) {
        final Dialog dialog = new Dialog(activity);
//        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.dialog_quote_completed);
        ImageView cros = dialog.findViewById(R.id.close);
        cros.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                Intent intent = new Intent(activity, ExploreServicesActivity.class);
                activity.startActivity(intent);
                activity.finish();
            }
        });
        FloatingActionButton dialogButton = dialog.findViewById(R.id.done_post_fab);
        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                Intent intent = new Intent(activity, ExploreServicesActivity.class);
                activity.startActivity(intent);
                activity.finishAffinity();
            }
        });

        dialog.show();

    }
}
