package com.sukhizindagi.app.ui.services

import okhttp3.ResponseBody
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.lang.Exception

class ServicesFactory(private val resultCallback: Result, private val requestCode: Int) : Callback<ResponseBody> {


    override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
        if (response.isSuccessful) {
            try {
                val jsonObject = JSONObject(response.body()!!.string())
                if (jsonObject.getBoolean("status"))
                    resultCallback.onSuccess(jsonObject.get("data").toString(), requestCode)
                else
                    resultCallback.onFailure(jsonObject.get("message").toString(), requestCode)
            } catch (e: Exception) {
                e.printStackTrace()
                resultCallback.onFailure(e.message ?: "Something went wrong", requestCode)
            }
        } else {
            resultCallback.onFailure("Something went wrong", requestCode)
        }
    }

    override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
        resultCallback.onFailure(t.message ?: "Something went wrong", requestCode)
    }

    fun signUp(email: String, password: String, device_type: String, app_id: String, mobile: String, firstname: String, lastname: String) {
        RetrofitClient.getInstance().create(EndPoints::class.java)
                .signUp(email, password, device_type, app_id, mobile, firstname, lastname)
                .enqueue(this)
    }

    fun signIn(email: String, password: String, device_type: String, app_id: String) {
        RetrofitClient.getInstance().create(EndPoints::class.java)
                .signIn(email, password, device_type, app_id)
                .enqueue(this)
    }

    fun changePassword(email: String, password: String, newPassword: String) {
        RetrofitClient.getInstance().create(EndPoints::class.java)
                .changePassword(email, password, newPassword)
                .enqueue(this)
    }

    fun forgotPassword(mobile: String) {

        RetrofitClient.getInstance().create(EndPoints::class.java)
                .forgetPassword(mobile)
                .enqueue(this)
    }

    fun resetPassword(mobile: String, otp: String, password: String) {

        RetrofitClient.getInstance().create(EndPoints::class.java)
                .resetPassword(mobile, otp, password)
                .enqueue(this)
    }

    fun postJob(email: String, category_id: String, password: String, job_title: String, total_hrs: String, total_maids: String, subscription_type: String, preferred_date: String, preferred_time: String, address: String, latitude: String, longitude: String) {
        RetrofitClient.getInstance().create(EndPoints::class.java)
                .postJob(email, password, category_id, job_title, total_hrs, total_maids, subscription_type, preferred_date, preferred_time, address, latitude, longitude)
                .enqueue(this)
    }

    fun postQuote(userName: String,  password: String,category_id: String, job_details:String, address: String, latitude: String, longitude: String) {
        RetrofitClient.getInstance().create(EndPoints::class.java)
                .postQuote(userName, password, category_id,  job_details,address, latitude, longitude)
                .enqueue(this)
    }

    fun sendOtp(mobile: String, otp: String) {
        RetrofitClient.getInstance().create(EndPoints::class.java)
                .sendOtp(mobile, otp)
                .enqueue(this)
    }

    fun ongoingJobs(email: String, password: String) {

        RetrofitClient.getInstance().create(EndPoints::class.java)
                .ongoingJobs(email, password)
                .enqueue(this)
    }

    fun pastJobs(email: String, password: String) {

        RetrofitClient.getInstance().create(EndPoints::class.java)
                .pastJobs(email, password)
                .enqueue(this)
    }

    fun getQuotes(email: String, password: String) {

        RetrofitClient.getInstance().create(EndPoints::class.java)
                .getQuotes(email, password)
                .enqueue(this)
    }

    fun feedBack(email: String, password: String, jobid: String, rating: String, feedBackString: String) {

        RetrofitClient.getInstance().create(EndPoints::class.java)
                .feedBack(email, password, jobid, rating, feedBackString)
                .enqueue(this)
    }

    fun getAllCuisines() {
        RetrofitClient.getInstance().create(EndPoints::class.java).getAllCuisines().enqueue(this)
    }


}