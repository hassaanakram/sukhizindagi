package com.sukhizindagi.app.ui;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.sukhizindagi.app.R;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private Button guestLoginBtn, signUpBtn, signInBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        statusBarColor();
        initView();
    }

    private void initView() {
        signUpBtn = findViewById(R.id.signUp);
        signInBtn = findViewById(R.id.signIn);
        signUpBtn.setOnClickListener(this);
        signInBtn.setOnClickListener(this);
        guestLoginBtn = findViewById(R.id.guestSignIn);
        guestLoginBtn.setOnClickListener(this);
    }

    private void statusBarColor() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            getWindow().setStatusBarColor(getResources().getColor(R.color.colorPrimaryDark, this.getTheme()));
        } else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().setStatusBarColor(getResources().getColor(R.color.colorPrimaryDark));
        }
    }

    Intent intent;

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.guestSignIn:
                moveForward(ExploreServicesActivity.class);
                break;
            case R.id.signUp:
                moveForward(SignUpActivity.class);
                break;
            case R.id.signIn:
                moveForward(SignInActivity.class);
                break;
        }
    }

    private void moveForward(Class<?> clas) {
        intent = new Intent(this, clas);
        startActivity(intent);
    }

}
