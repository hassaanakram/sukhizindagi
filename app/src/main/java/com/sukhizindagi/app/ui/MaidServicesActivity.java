package com.sukhizindagi.app.ui;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ExpandableListView;

import com.sukhizindagi.app.R;
import com.sukhizindagi.app.ui.adapters.ExpandableListaAdapter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class MaidServicesActivity extends AppCompatActivity implements ExpandableListView.OnChildClickListener {
    ExpandableListaAdapter listAdapter;
    ExpandableListView expListView;
    List<String> listDataHeader;
    HashMap<String, List<String>> listDataChild;
    Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maid_services);
        initView();
    }

    @SuppressLint("ClickableViewAccessibility")
    private void initView() {
        // get the listview
        expListView = findViewById(R.id.lvExp);
//        expListView.setOnTouchListener(this);
        expListView.setOnChildClickListener(this);
        // preparing list data
        prepareListData();

        listAdapter = new ExpandableListaAdapter(this, listDataHeader, listDataChild);

        // setting list adapter
        expListView.setAdapter(listAdapter);
        toolbar = findViewById(R.id.toolbar_chose_location);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    private void prepareListData() {
        listDataHeader = new ArrayList<String>();
        listDataChild = new HashMap<String, List<String>>();

        // Adding child data
        listDataHeader.add("General Home Cleaning");
        listDataHeader.add("Laundry");
        listDataHeader.add("Cook / Helper");
        listDataHeader.add("Ironing");

        // Adding child data
        List<String> GeneralHomeCleaning = new ArrayList<String>();
        GeneralHomeCleaning.add("Rooms, Kitchen and Bathroom cleaning");

        List<String> Laundry = new ArrayList<String>();
        Laundry.add("Hand washing, machine washing and drying clothes");

        List<String> Ironing = new ArrayList<String>();
        Ironing.add("Full meal cooking, cutting fruits & vegetables and Bread (Chappati) making");

        List<String> DishWashing = new ArrayList<String>();
        DishWashing.add("Clothes Ironing and Hanging");


        listDataChild.put(listDataHeader.get(0), GeneralHomeCleaning); // Header, Child data
        listDataChild.put(listDataHeader.get(1), Laundry);
        listDataChild.put(listDataHeader.get(2), Ironing);
        listDataChild.put(listDataHeader.get(3), DishWashing);
    }



    @Override
    public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, int childPosition, long id) {
        final String selected = (String) listAdapter.getChild(
                groupPosition, childPosition);

        Intent intent = new Intent();
        switch (selected) {

        }
        return true;
    }
}
