package com.sukhizindagi.app.ui;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.cardview.widget.CardView;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.viewpager.widget.ViewPager;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.daimajia.slider.library.SliderTypes.BaseSliderView;
import com.daimajia.slider.library.Tricks.ViewPagerEx;
import com.google.android.material.navigation.NavigationView;
import com.google.android.material.tabs.TabLayout;
import com.google.gson.Gson;
import com.sukhizindagi.app.GardeningActivity;
import com.sukhizindagi.app.R;
import com.sukhizindagi.app.ui.adapters.MyPager;
import com.sukhizindagi.app.ui.models.User;

import org.json.JSONObject;

import java.util.Timer;
import java.util.TimerTask;

import javax.security.auth.login.LoginException;

public class ExploreServicesActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener, View.OnClickListener, BaseSliderView.OnSliderClickListener, ViewPagerEx.OnPageChangeListener {

    private ViewPager mDemoSlider;
    private TabLayout tlIndicators;
    private CardView maidCard, gardeningCard, handymandCard, sprayingCard;
    TextView userName;
    String nameOfUser;
    NavigationView navigationView;
    Timer timer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_explore_services);

        initView();
        settingSlider();
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    User userObj;
    Gson gson = new Gson();

    private void initView() {
        navigationView = findViewById(R.id.nav_view);
        mDemoSlider = findViewById(R.id.slider);
        tlIndicators = findViewById(R.id.tlIndicators);
        maidCard = findViewById(R.id.maid_card);
        gardeningCard = findViewById(R.id.gardening_card);
        gardeningCard.setOnClickListener(this);
        handymandCard = findViewById(R.id.handymand_card);
        sprayingCard = findViewById(R.id.spraying_cards);
        sprayingCard.setOnClickListener(this);
        handymandCard.setOnClickListener(this);
        maidCard.setOnClickListener(this);
        try {
            String userString = PreferenceManager.getDefaultSharedPreferences(this).getString("user", "a");
            JSONObject jsonObject = new JSONObject(userString);
            userObj = gson.fromJson(jsonObject.getString("user"), User.class);
        } catch (Exception e) {
            e.printStackTrace();
        }
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        DrawerLayout drawer = findViewById(R.id.drawer);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open,
                R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        View headerView = navigationView.getHeaderView(0);
        navigationView.setNavigationItemSelectedListener(this);
        userName = headerView.findViewById(R.id.user_name);
        userName.setOnClickListener(this);
        if (userObj != null) {
            userName.setText(userObj.getDisplayName());
        }
    }

    final Handler handler = new Handler();
    private int currentPage = 0;

    private void settingSlider() {
        mDemoSlider.setAdapter(new MyPager(this));
        tlIndicators.setupWithViewPager(mDemoSlider);
        final Runnable Update = new Runnable() {
            public void run() {
                if (currentPage == 3) {
                    currentPage = 0;
                }
                mDemoSlider.setCurrentItem(currentPage++, true);
            }
        };

        timer = new Timer();
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                handler.post(Update);
            }
        }, 500, 4000);

    }

    @Override
    protected void onStop() {
        // To prevent a memory leak on rotation, make sure to call stopAutoCycle() on the slider before activity or fragment is destroyed
//        mDemoSlider.stopAutoCycle();
        super.onStop();
    }

    @Override
    public void onSliderClick(BaseSliderView slider) {
        Toast.makeText(this, slider.getBundle().get("extra") + "", Toast.LENGTH_SHORT).show();
    }


    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
    }

    @Override
    public void onPageSelected(int position) {

        Log.d("Slider Demo", "Page Changed: " + position);
    }

    @Override
    public void onPageScrollStateChanged(int state) {
    }

    Intent intent;

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.maid_card:
                intent = new Intent(ExploreServicesActivity.this, MaidServicesActivity.class);
                startActivity(intent);
                break;
            case R.id.user_name:
                if (userName.getText().toString().equals("Please Log In")) {
                    startActivity(new Intent(ExploreServicesActivity.this, MainActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK));
                }
                break;
            case R.id.gardening_card:
                if (userObj != null) {
                    intent = new Intent(ExploreServicesActivity.this, GardeningActivity.class);
                    startActivity(intent);
                } else {
                    Toast.makeText(this, "Kindly Login to see this feature", Toast.LENGTH_SHORT).show();
                }
                break;
            case R.id.handymand_card:
                if (userObj != null) {
                    intent = new Intent(ExploreServicesActivity.this, EventManagementActivity.class);
                    startActivity(intent);
                } else {
                    Toast.makeText(this, "Kindly Login to see this feature", Toast.LENGTH_SHORT).show();
                }
                break;
            case R.id.spraying_cards:
                if (userObj != null) {
                    intent = new Intent(ExploreServicesActivity.this, SprayingActivity.class);
                    startActivity(intent);
                } else {
                    Toast.makeText(this, "Kindly Login to see this feature", Toast.LENGTH_SHORT).show();
                }
                break;
        }
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {

        // Handle navigation view item clicks here.
        int id = menuItem.getItemId();
        if (id == R.id.nav_home) {
            // Handle the camera action
            menuItem.setCheckable(true);
            menuItem.setChecked(true);
        } else if (id == R.id.nav_job_record) {
            if (userObj != null) {
                Intent intent = new Intent(ExploreServicesActivity.this, JobRecordsActivity.class);
                startActivity(intent);
            } else {
                Toast.makeText(this, "Please Sign In to continue... ", Toast.LENGTH_SHORT).show();
                return false;
            }
        } else if (id == R.id.nav_quotes) {
            if (userObj != null) {
                Intent intent = new Intent(ExploreServicesActivity.this, QuotesActivity.class);
                startActivity(intent);
            } else {
                Toast.makeText(this, "Please Sign In to continue... ", Toast.LENGTH_SHORT).show();
                return false;
            }
        } else if (id == R.id.Settings) {
            if (userObj != null) {
                Intent intent = new Intent(ExploreServicesActivity.this, SettingsActivity.class);
                startActivity(intent);
            } else {
                Toast.makeText(this, "Please Sign In to continue...", Toast.LENGTH_SHORT).show();
                return false;
            }
        } else if (id == R.id.fb) {
            Uri uri = Uri.parse("https://www.facebook.com/sukhizindagipk/"); // missing 'http://' will cause crashed
            Intent intent = new Intent(Intent.ACTION_VIEW, uri);
            startActivity(intent);
        } else if (id == R.id.insta) {
            Uri uri = Uri.parse("https://www.instagram.com/sukhizindagi/ "); // missing 'http://' will cause crashed
            Intent intent = new Intent(Intent.ACTION_VIEW, uri);
            startActivity(intent);
        } else if (id == R.id.terms_id) {
            Uri uri = Uri.parse("https://sukhizindagi.com/terms-and-conditions/"); // missing 'http://' will cause crashed
            Intent intent = new Intent(Intent.ACTION_VIEW, uri);
            startActivity(intent);
        } else if (id == R.id.privacy_id) {
            Uri uri = Uri.parse("https://sukhizindagi.com/privacy-policy/"); // missing 'http://' will cause crashed
            Intent intent = new Intent(Intent.ACTION_VIEW, uri);
            startActivity(intent);

        } else if (id == R.id.nav_support) {
            Uri uri = Uri.parse("https://sukhizindagi.com/contact-us/"); // missing 'http://' will cause crashed
            Intent intent = new Intent(Intent.ACTION_VIEW, uri);
            startActivity(intent);

        } else if (id == R.id.nav_rate) {
            try {
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + getPackageName())));
            } catch (ActivityNotFoundException e) {
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + getPackageName())));
            }
        }
        DrawerLayout drawer = findViewById(R.id.drawer);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

}
