package com.sukhizindagi.app.ui;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.cardview.widget.CardView;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.sukhizindagi.app.R;
import com.sukhizindagi.app.ui.models.User;

import org.json.JSONObject;

public class SettingsActivity extends AppCompatActivity implements View.OnClickListener {
    Toolbar toolbar;
    CardView updatePasswordCard, logoutCard;
    TextView namTv, emailTv, phoneTv;
    String name, email, phone;
    User userObj;
    Gson gson = new Gson();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);
        initView();
    }

    private void initView() {
        try {
            String userString = PreferenceManager.getDefaultSharedPreferences(this).getString("user", null);
            JSONObject jsonObject = new JSONObject(userString);
            userObj = gson.fromJson(jsonObject.getString("user"), User.class);
        } catch (Exception e) {
            e.printStackTrace();
        }
        namTv = findViewById(R.id.fullname_tv);
        emailTv = findViewById(R.id.email_tv);
        phoneTv = findViewById(R.id.phone_tv);
        if (userObj != null) {
            namTv.setText(userObj.getDisplayName());
            emailTv.setText(userObj.getEmail());
            String mobile = userObj.getMobile();
            if(!mobile.startsWith("0") && !mobile.startsWith("+92") && !mobile.startsWith("92")){
                mobile = "0" + mobile;
            }
            phoneTv.setText(mobile);
        }
        updatePasswordCard = findViewById(R.id.change_update_password_card);
        updatePasswordCard.setOnClickListener(this);
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SettingsActivity.this, ExploreServicesActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);

                finish();            }
        });
        logoutCard = findViewById(R.id.your_logout_card);
        logoutCard.setOnClickListener(this);
    }

    @SuppressLint("CommitPrefEdits")
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.change_update_password_card:
                Intent intent = new Intent(SettingsActivity.this, UpdatePasswordActivity.class);
                startActivity(intent);
                break;
            case R.id.your_logout_card:
                Toast.makeText(this, "Logout Successful. Kindly signin again to continue.", Toast.LENGTH_SHORT).show();
                PreferenceManager.getDefaultSharedPreferences(this).edit().clear().apply();
                SharedPreferences settings = this.getSharedPreferences("user", MODE_PRIVATE);
                settings.edit().clear().apply();
                SharedPreferences email = this.getSharedPreferences("user_email", MODE_PRIVATE);
                email.edit().clear().apply();
                SharedPreferences pass = this.getSharedPreferences("password", MODE_PRIVATE);
                pass.edit().clear().apply();
                intent = new Intent(SettingsActivity.this, MainActivity.class);
                startActivity(intent);
                finish();
        }
    }
}
