package com.sukhizindagi.app.ui.helpers;

import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.sukhizindagi.app.R;
import com.sukhizindagi.app.ui.ExploreServicesActivity;

public class DialogHelper {


    public void showDialog(final Activity activity) {
        final Dialog dialog = new Dialog(activity);
//        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.dialog_box);
        ImageView cros = dialog.findViewById(R.id.close);
        cros.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                Intent intent = new Intent(activity, ExploreServicesActivity.class);
                activity.startActivity(intent);
                activity.finish();
            }
        });
        FloatingActionButton dialogButton = dialog.findViewById(R.id.done_post_fab);
        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                Intent intent = new Intent(activity, ExploreServicesActivity.class);
                activity.startActivity(intent);
                activity.finishAffinity();
            }
        });

        dialog.show();

    }

    public static void showPermissionDialog(final Activity activity, final DialogButtonsListener<Boolean> listener) {
        final Dialog dialog = new Dialog(activity);
//        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.dialog_permission_msg);
//        ImageView ivLogo = dialog.findViewById(R.id.ivLogo);
//        cros.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                dialog.dismiss();
//                Intent intent = new Intent(activity, ExploreServicesActivity.class);
//                activity.startActivity(intent);
//                activity.finish();
//            }
//        });
        Button allowPermissionBtn = dialog.findViewById(R.id.allowPermission);
        allowPermissionBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
               listener.positiveButtonClick(true);
            }
        });
        dialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialogInterface) {
                listener.canceled(null);
            }
        });

        dialog.show();

    }

    public interface DialogButtonsListener<T>{
        void positiveButtonClick(T t);
        void negativeButtonClick(T t);
        void canceled(T t);
    }
}