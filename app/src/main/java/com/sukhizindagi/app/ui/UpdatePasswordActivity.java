package com.sukhizindagi.app.ui;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.cardview.widget.CardView;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.sukhizindagi.app.R;
import com.sukhizindagi.app.ui.helpers.Utils;
import com.sukhizindagi.app.ui.services.Result;
import com.sukhizindagi.app.ui.services.ServicesFactory;

import org.jetbrains.annotations.NotNull;


public class UpdatePasswordActivity extends AppCompatActivity implements View.OnClickListener, Result {
    Toolbar toolbar;
    CardView updatePasswordCard;
    String email, password, newPassword, confirmNewPassword;
    EditText confirmPasswordEt, passwordEt;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update_password);
        initView();
    }

    private void initView() {
        updatePasswordCard = findViewById(R.id.update_password_card);
        updatePasswordCard.setOnClickListener(this);
        confirmPasswordEt = findViewById(R.id.confirm_new_password_tv);
        passwordEt = findViewById(R.id.new_password_tv);
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        email = PreferenceManager.getDefaultSharedPreferences(this).getString("user_email", "email");
        password = PreferenceManager.getDefaultSharedPreferences(this).getString("password", "password");

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(UpdatePasswordActivity.this, ExploreServicesActivity.class);
                startActivity(intent);
                finish();
            }
        });
    }

    private Boolean areInputsOkay() {
        if (TextUtils.isEmpty(passwordEt.getText())) {
            passwordEt.setError("Required");
            passwordEt.requestFocus();
            return false;
        } else if (passwordEt.getText().length() < 6) {
            passwordEt.setError("Password is too short");
            passwordEt.requestFocus();
            return false;
        } else if (TextUtils.isEmpty(confirmPasswordEt.getText())) {
            confirmPasswordEt.setError("Required");
            confirmPasswordEt.requestFocus();
            return false;
        } else if (!passwordEt.getText().toString().equals(confirmPasswordEt.getText().toString())) {
            Toast.makeText(this, "Password does'nt match", Toast.LENGTH_SHORT).show();
            return false;
        }
        newPassword = passwordEt.getText().toString();
        confirmNewPassword = confirmPasswordEt.getText().toString();
        return true;
    }

    Dialog dialog;

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.update_password_card:
                if (areInputsOkay()) {
                    dialog = Utils.showLoadingDialog(this);
                    new ServicesFactory(this, 0).changePassword(email, password, newPassword);
                }
                break;
        }

    }

    @Override
    public void onSuccess(@NotNull String data, int requestCode) {
        Utils.dismissDialog(dialog);
        Toast.makeText(this, "Password Updated Successfully", Toast.LENGTH_SHORT).show();
        Intent intent = new Intent(UpdatePasswordActivity.this, SettingsActivity.class);
        startActivity(intent);
        finish();
    }

    @Override
    public void onFailure(@NotNull String cause, int requestCode) {
        Utils.dismissDialog(dialog);
        Toast.makeText(this, cause, Toast.LENGTH_SHORT).show();
    }
}
