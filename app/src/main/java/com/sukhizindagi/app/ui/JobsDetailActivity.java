package com.sukhizindagi.app.ui;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.cardview.widget.CardView;
import androidx.core.content.ContextCompat;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.sukhizindagi.app.R;
import com.sukhizindagi.app.ui.helpers.DialogHelper;
import com.sukhizindagi.app.ui.helpers.FeedBackDialog;
import com.sukhizindagi.app.ui.models.JobsModel;


public class JobsDetailActivity extends AppCompatActivity implements View.OnClickListener {
    Toolbar toolbar;
    Intent intent;
    JobsModel jobsModel;
    TextView job_tv, time_tv, date_tv, status_tv, location_tv, maid_id_tv, total_bill_tv;
    CardView leaveFeedback;
    Boolean isFromOngoing = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_jobs_detail);
        initView();
    }

    private void initView() {
        intent = getIntent();
        if (intent != null) {
            jobsModel = (JobsModel) intent.getSerializableExtra("obj");
            isFromOngoing = intent.getBooleanExtra("isFromOngoing", false);
            jobsModel.getAddress();
        }
        toolbar = findViewById(R.id.toolbar_job_detals);

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(JobsDetailActivity.this, JobRecordsActivity.class);
                startActivity(intent);
                finish();
            }
        });
        leaveFeedback = findViewById(R.id.leave_feedback_card);
        leaveFeedback.setOnClickListener(this);
        job_tv = findViewById(R.id.job_tv);
        time_tv = findViewById(R.id.time_tv);
        date_tv = findViewById(R.id.date_tv);
        status_tv = findViewById(R.id.status_tv);
        location_tv = findViewById(R.id.location_tv);
        maid_id_tv = findViewById(R.id.maid_id_tv);
        total_bill_tv = findViewById(R.id.total_bill_tv);
        job_tv.setText(jobsModel.getJobTitle());
        time_tv.setText(jobsModel.getConfTime());
        date_tv.setText(jobsModel.getConfDate());
        status_tv.setText(jobsModel.getJobStatus());
        location_tv.setText(jobsModel.getAddress());
        maid_id_tv.setText(jobsModel.getExpertName()+"");
        total_bill_tv.setText("Pkr. "+jobsModel.getJobAmount());
        if (isFromOngoing) {
            leaveFeedback.setVisibility(View.GONE);
        } else if (jobsModel.getExpertRating() != null && !jobsModel.getExpertRating().isEmpty() && !jobsModel.getExpertRating().equals("0")) {
            leaveFeedback.setBackgroundColor(ContextCompat.getColor(getApplicationContext(), R.color.tab_bar_light_black));
            leaveFeedback.setEnabled(false);
        }

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.leave_feedback_card:
                if (isFromOngoing) {
                    return;
                } else {

                    FeedBackDialog alert = new FeedBackDialog();
                    alert.showDialog(JobsDetailActivity.this, jobsModel.getId());
                }
                break;
        }
    }
}
