
package com.sukhizindagi.app.ui.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class User {

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("app_id")
    @Expose
    private String appId;
    @SerializedName("user_name")
    @Expose
    private String userName;
    @SerializedName("display_name")
    @Expose
    private String displayName;
    @SerializedName("password")
    @Expose
    private String password;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("mobile")
    @Expose
    private String mobile;
    @SerializedName("activation_token")
    @Expose
    private Object activationToken;
    @SerializedName("last_activation_request")
    @Expose
    private Object lastActivationRequest;
    @SerializedName("lost_password_request")
    @Expose
    private Object lostPasswordRequest;
    @SerializedName("active")
    @Expose
    private String active;
    @SerializedName("title")
    @Expose
    private Object title;
    @SerializedName("sign_up_stamp")
    @Expose
    private String signUpStamp;
    @SerializedName("last_sign_in_stamp")
    @Expose
    private String lastSignInStamp;
    @SerializedName("otp")
    @Expose
    private String otp;
    @SerializedName("date_of_otp")
    @Expose
    private String dateOfOtp;
    @SerializedName("user_type")
    @Expose
    private String userType;
    @SerializedName("display_picture")
    @Expose
    private Object displayPicture;
    @SerializedName("verifystatus")
    @Expose
    private String verifystatus;
    @SerializedName("address")
    @Expose
    private Object address;
    @SerializedName("city")
    @Expose
    private Object city;
    @SerializedName("pincode")
    @Expose
    private Object pincode;
    @SerializedName("state")
    @Expose
    private Object state;
    @SerializedName("date_of_register")
    @Expose
    private String dateOfRegister;
    @SerializedName("date_of_close")
    @Expose
    private Object dateOfClose;
    @SerializedName("upline")
    @Expose
    private String upline;
    @SerializedName("latitude")
    @Expose
    private String latitude;
    @SerializedName("longitude")
    @Expose
    private String longitude;
    @SerializedName("google_auth")
    @Expose
    private Object googleAuth;
    @SerializedName("facebook_oauth")
    @Expose
    private Object facebookOauth;
    @SerializedName("job_progress")
    @Expose
    private Object jobProgress;
    @SerializedName("cnic")
    @Expose
    private Object cnic;
    @SerializedName("medical")
    @Expose
    private Object medical;
    @SerializedName("interview_date")
    @Expose
    private Object interviewDate;
    @SerializedName("interview_timing")
    @Expose
    private Object interviewTiming;
    @SerializedName("occupation")
    @Expose
    private Object occupation;
    @SerializedName("experience")
    @Expose
    private Object experience;
    @SerializedName("references_num1")
    @Expose
    private Object referencesNum1;
    @SerializedName("intrested")
    @Expose
    private Object intrested;
    @SerializedName("references_num2")
    @Expose
    private Object referencesNum2;
    @SerializedName("assigned_category")
    @Expose
    private Object assignedCategory;
    @SerializedName("role_type")
    @Expose
    private Object roleType;
    @SerializedName("referral_code")
    @Expose
    private Object referralCode;
    @SerializedName("refer_code")
    @Expose
    private Integer referCode;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getAppId() {
        return appId;
    }

    public void setAppId(String appId) {
        this.appId = appId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public Object getActivationToken() {
        return activationToken;
    }

    public void setActivationToken(Object activationToken) {
        this.activationToken = activationToken;
    }

    public Object getLastActivationRequest() {
        return lastActivationRequest;
    }

    public void setLastActivationRequest(Object lastActivationRequest) {
        this.lastActivationRequest = lastActivationRequest;
    }

    public Object getLostPasswordRequest() {
        return lostPasswordRequest;
    }

    public void setLostPasswordRequest(Object lostPasswordRequest) {
        this.lostPasswordRequest = lostPasswordRequest;
    }

    public String getActive() {
        return active;
    }

    public void setActive(String active) {
        this.active = active;
    }

    public Object getTitle() {
        return title;
    }

    public void setTitle(Object title) {
        this.title = title;
    }

    public String getSignUpStamp() {
        return signUpStamp;
    }

    public void setSignUpStamp(String signUpStamp) {
        this.signUpStamp = signUpStamp;
    }

    public String getLastSignInStamp() {
        return lastSignInStamp;
    }

    public void setLastSignInStamp(String lastSignInStamp) {
        this.lastSignInStamp = lastSignInStamp;
    }

    public String getOtp() {
        return otp;
    }

    public void setOtp(String otp) {
        this.otp = otp;
    }

    public String getDateOfOtp() {
        return dateOfOtp;
    }

    public void setDateOfOtp(String dateOfOtp) {
        this.dateOfOtp = dateOfOtp;
    }

    public String getUserType() {
        return userType;
    }

    public void setUserType(String userType) {
        this.userType = userType;
    }

    public Object getDisplayPicture() {
        return displayPicture;
    }

    public void setDisplayPicture(Object displayPicture) {
        this.displayPicture = displayPicture;
    }

    public String getVerifystatus() {
        return verifystatus;
    }

    public void setVerifystatus(String verifystatus) {
        this.verifystatus = verifystatus;
    }

    public Object getAddress() {
        return address;
    }

    public void setAddress(Object address) {
        this.address = address;
    }

    public Object getCity() {
        return city;
    }

    public void setCity(Object city) {
        this.city = city;
    }

    public Object getPincode() {
        return pincode;
    }

    public void setPincode(Object pincode) {
        this.pincode = pincode;
    }

    public Object getState() {
        return state;
    }

    public void setState(Object state) {
        this.state = state;
    }

    public String getDateOfRegister() {
        return dateOfRegister;
    }

    public void setDateOfRegister(String dateOfRegister) {
        this.dateOfRegister = dateOfRegister;
    }

    public Object getDateOfClose() {
        return dateOfClose;
    }

    public void setDateOfClose(Object dateOfClose) {
        this.dateOfClose = dateOfClose;
    }

    public String getUpline() {
        return upline;
    }

    public void setUpline(String upline) {
        this.upline = upline;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public Object getGoogleAuth() {
        return googleAuth;
    }

    public void setGoogleAuth(Object googleAuth) {
        this.googleAuth = googleAuth;
    }

    public Object getFacebookOauth() {
        return facebookOauth;
    }

    public void setFacebookOauth(Object facebookOauth) {
        this.facebookOauth = facebookOauth;
    }

    public Object getJobProgress() {
        return jobProgress;
    }

    public void setJobProgress(Object jobProgress) {
        this.jobProgress = jobProgress;
    }

    public Object getCnic() {
        return cnic;
    }

    public void setCnic(Object cnic) {
        this.cnic = cnic;
    }

    public Object getMedical() {
        return medical;
    }

    public void setMedical(Object medical) {
        this.medical = medical;
    }

    public Object getInterviewDate() {
        return interviewDate;
    }

    public void setInterviewDate(Object interviewDate) {
        this.interviewDate = interviewDate;
    }

    public Object getInterviewTiming() {
        return interviewTiming;
    }

    public void setInterviewTiming(Object interviewTiming) {
        this.interviewTiming = interviewTiming;
    }

    public Object getOccupation() {
        return occupation;
    }

    public void setOccupation(Object occupation) {
        this.occupation = occupation;
    }

    public Object getExperience() {
        return experience;
    }

    public void setExperience(Object experience) {
        this.experience = experience;
    }

    public Object getReferencesNum1() {
        return referencesNum1;
    }

    public void setReferencesNum1(Object referencesNum1) {
        this.referencesNum1 = referencesNum1;
    }

    public Object getIntrested() {
        return intrested;
    }

    public void setIntrested(Object intrested) {
        this.intrested = intrested;
    }

    public Object getReferencesNum2() {
        return referencesNum2;
    }

    public void setReferencesNum2(Object referencesNum2) {
        this.referencesNum2 = referencesNum2;
    }

    public Object getAssignedCategory() {
        return assignedCategory;
    }

    public void setAssignedCategory(Object assignedCategory) {
        this.assignedCategory = assignedCategory;
    }

    public Object getRoleType() {
        return roleType;
    }

    public void setRoleType(Object roleType) {
        this.roleType = roleType;
    }

    public Object getReferralCode() {
        return referralCode;
    }

    public void setReferralCode(Object referralCode) {
        this.referralCode = referralCode;
    }

    public Integer getReferCode() {
        return referCode;
    }

    public void setReferCode(Integer referCode) {
        this.referCode = referCode;
    }

}
