package com.sukhizindagi.app;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.FragmentTransaction;

import android.app.Dialog;
import android.os.Bundle;
import android.os.RecoverySystem;
import android.preference.PreferenceManager;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.model.LatLng;
import com.google.gson.Gson;
import com.sukhizindagi.app.ui.PreviewBookingActivity;
import com.sukhizindagi.app.ui.fragments.CoomonMapsFragment;
import com.sukhizindagi.app.ui.helpers.DialogHelper;
import com.sukhizindagi.app.ui.helpers.DialogQuote;
import com.sukhizindagi.app.ui.helpers.Utils;
import com.sukhizindagi.app.ui.models.User;
import com.sukhizindagi.app.ui.services.Result;
import com.sukhizindagi.app.ui.services.ServicesFactory;

import org.jetbrains.annotations.NotNull;
import org.json.JSONObject;

public class CommonGEMapActivity extends AppCompatActivity implements Result, View.OnClickListener {
    private CoomonMapsFragment mapFragment = new CoomonMapsFragment();
    private TextView adressTv;
    private Toolbar toolbar;
    private CardView requestQuote;
    String categoryId;
    User userObj;
    Gson gson = new Gson();
    String password;
    private EditText manualAddress;
    private String job_details = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_common_gemap);
        password = PreferenceManager.getDefaultSharedPreferences(this).getString("password", null);
        categoryId = getIntent().getStringExtra("categoryId");
        job_details = getIntent().getStringExtra("additionalDetailsString");
        try {
            String userString = PreferenceManager.getDefaultSharedPreferences(this).getString("user", null);
            JSONObject jsonObject = new JSONObject(userString);
            userObj = gson.fromJson(jsonObject.getString("user"), User.class);
        } catch (Exception e) {
            e.printStackTrace();
        }
        initView();

    }

    private void initView() {
        manualAddress = findViewById(R.id.manual_adress);
        final FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.add(R.id.chose_location_frame, mapFragment);
        requestQuote = findViewById(R.id.requestQuote);
        requestQuote.setOnClickListener(this);
        transaction.replace(R.id.chose_location_frame, mapFragment);
        transaction.commit();
        adressTv = findViewById(R.id.address_from_frag);
        toolbar = findViewById(R.id.toolbar_chose_location);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

    }

    Dialog dialog;
    LatLng latLng;

    public void getAddressFromFragment(String address, String city, String state, String country, LatLng jobLocation) {
        addressOfUserFromMap = address;
        adressTv.setText(address);
        latLng = jobLocation;
    }

    @Override
    public void onSuccess(@NotNull String data, int requestCode) {
        Utils.dismissDialog(dialog);
        DialogQuote alert = new DialogQuote();
        alert.showDialog(CommonGEMapActivity.this);
    }

    @Override
    public void onFailure(@NotNull String cause, int requestCode) {

        Utils.dismissDialog(dialog);
        Toast.makeText(this, cause, Toast.LENGTH_SHORT).show();
    }

    String addressOfUserFromMap;

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.requestQuote:
                if (TextUtils.isEmpty(manualAddress.getText())) {
                    dialog = Utils.showLoadingDialog(this);
                    if (job_details != null)
                        new ServicesFactory(this, 0).postQuote(userObj.getEmail(), password, categoryId, job_details, addressOfUserFromMap, String.valueOf(latLng.latitude), String.valueOf(latLng.longitude));
                    else
                        new ServicesFactory(this, 0).postQuote(userObj.getEmail(), password, categoryId, "", addressOfUserFromMap, String.valueOf(latLng.latitude), String.valueOf(latLng.longitude));

                } else {
                    dialog = Utils.showLoadingDialog(this);
                    if (job_details!=null)
                    new ServicesFactory(this, 0).postQuote(userObj.getEmail(), password, categoryId, job_details, addressOfUserFromMap + "/" + manualAddress.getText().toString(), String.valueOf(latLng.latitude), String.valueOf(latLng.longitude));
                   else
                    new ServicesFactory(this, 0).postQuote(userObj.getEmail(), password, categoryId, "", addressOfUserFromMap + "/" + manualAddress.getText().toString(), String.valueOf(latLng.latitude), String.valueOf(latLng.longitude));

                }

                break;
        }
    }
}
